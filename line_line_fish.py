#!/usr/bin/env python

import sys

from functions.text.logging import print_log
from interface.commands import process_login

if __name__ == "__main__":
    process_login(sys.argv)
    print_log()
