from objects.data.sea_objects.sea_object_factory import SeaObjectFactory
from objects.data.very_simple_q import VerySimpleQueue


class CatchQueue(VerySimpleQueue):

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        queue = []
        for item in contents.get('queue'):
            queue.append(SeaObjectFactory.make_from_dict(item))

        catch_queue = CatchQueue()
        catch_queue.contents = queue
        return catch_queue

    def to_dict(self):
        """Return this object as a dict."""

        queue = []

        for item in self.contents:
            queue.append(item.to_dict())

        return {'queue': queue}
