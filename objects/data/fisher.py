from functions.state.serialization import dump_object
from functions.text.formatting import roman
from objects.data.catch_queue import CatchQueue
from objects.data.fish_data.fish import Fish
from objects.data.items.inventory import Inventory
from objects.data.location.location import Location
from objects.data.time_tokens import TimeTokens
from objects.info.feat_book import FeatBook
from objects.info.item_info import all_items
from objects.info.item_type import ItemType
from objects.info.recipe_book import RecipeBook


class Fisher:
    savable_keywords = ['money', 'caught', 'weight', 'mermaid', 'luck', 'discount',
                        'biggest', 'prized', 'history', 'chests', 'iridescent', 'whale_shark',
                        'newborns', 'biggest_net',
                        'ship_size', 'venom', 'wish_points', 'location',
                        'treasure_misses', 'treasure_boost', 'statue_challenge', 'chum', 'favour',
                        'name', 'lives', 'semaphore',
                        'inventory', 'feat_book', 'recipe_book', 'catch_queue', 'time_tokens']

    def __init__(self, contents=None):
        # Non-Saved Values
        self.anti_shark = 0
        self.burger = False
        self.flakes = False
        self.location_object = Location.definitive()

        # Original Game Stats
        self.money = None
        self.caught = None
        self.weight = None
        self.mermaid = None
        self.luck = None
        self.discount = None

        # Catch History
        self.biggest = None
        self.prized = None
        self.history = None
        self.chests = None
        self.iridescent = None
        self.whale_shark = None
        self.newborns = None
        self.biggest_net = None

        # Fisher and Ship Data
        self.ship_size = None
        self.venom = None
        self.wish_points = None
        self.location = None

        # Special Item Data
        self.treasure_misses = None
        self.treasure_boost = None
        self.statue_challenge = None
        self.chum = None
        self.favour = None

        # Player Communication
        self.name = None
        self.lives = None
        self.semaphore = None

        # Large Game Data Structures
        self.inventory = None
        self.feat_book = None
        self.recipe_book = None
        self.catch_queue = None
        self.time_tokens = None

        if contents:
            self.load_from_dict(contents)

    def init_fisher(self, contents):
        """Initialize the fisher with actual contents."""

        for keyword in Fisher.savable_keywords:
            try:
                self.__dict__[keyword] = (
                        contents.get(keyword) or Fisher.definitive().keyword_value(keyword)
                )
            except (RuntimeError, ValueError, AttributeError, TypeError, IndexError):
                self.__dict__[keyword] = Fisher.definitive().keyword_value(keyword)

    def keyword_value(self, keyword):
        """Return the fisher's value for a keyword."""

        return self.__dict__[keyword]

    def get_descriptor(self):
        """Return this fisher's full descriptor text."""

        if self.feat_book.count_achieved():
            feat_text = f' ({self.feat_book.count_achieved()} Feats)'
        else:
            feat_text = ''

        if self.semaphore:
            semaphore_text = f': [{self.semaphore}]'
        else:
            semaphore_text = ''

        return f'{self.name} {roman(self.lives)}{feat_text}{semaphore_text}'

    @staticmethod
    def definitive():
        """Return the standard starting Fisher."""

        default_fisher = Fisher()
        default_fisher.money = 0.00
        default_fisher.caught = 0
        default_fisher.weight = 0
        default_fisher.mermaid = ''
        default_fisher.luck = 0
        default_fisher.discount = 0

        default_fisher.biggest = Fish('Fish Fillet', 0.5, 1.99, 0)
        default_fisher.prized = Fish('Beloved Pet Goldfish', 0.1, 3.50, 0)
        default_fisher.history = {'Imaginary Fish': 0}
        default_fisher.chests = 0
        default_fisher.iridescent = 0
        default_fisher.whale_shark = 0.00
        default_fisher.newborns = []
        default_fisher.biggest_net = (0.00, 0)

        default_fisher.treasure_misses = 0
        default_fisher.treasure_boost = 0
        default_fisher.statue_challenge = False
        default_fisher.chum = 0
        default_fisher.favour = False

        default_fisher.name = None
        default_fisher.lives = 1
        default_fisher.semaphore = None

        default_fisher.ship_size = 10
        default_fisher.venom = 0
        default_fisher.wish_points = 0
        default_fisher.location = 'Safe Harbour'

        default_fisher.inventory = Inventory(
            {
                'Fishing License': all_items.get('Fishing License'),
                'Lunch Box': {
                    'use_type': ItemType.NO_TYPE, 'buy': 0, 'sell': 12,
                    'text': 'Contains your lunch',
                    'flavour':
                        'Mayo, pickles, and tuna on rye.  Surprisingly retail-able.'
                }
            }
        )
        default_fisher.feat_book = FeatBook()
        default_fisher.recipe_book = RecipeBook()
        default_fisher.catch_queue = CatchQueue()
        default_fisher.time_tokens = TimeTokens()

        return default_fisher

    def load_from_dict(self, contents):
        """Initialize the fisher with a contents dict."""

        for keyword in Fisher.savable_keywords:
                self.__dict__[keyword] = (
                        contents.get(keyword) or Fisher.definitive().keyword_value(keyword)
                )

        if contents.get('biggest'):
            self.biggest = Fish.from_dict(contents.get('biggest'))
        else:
            self.biggest = Fisher.definitive().biggest
        if contents.get('prized'):
            self.prized = Fish.from_dict(contents.get('prized'))
        else:
            self.prized = Fisher.definitive().prized

        if contents.get('inventory'):
            self.inventory = Inventory.from_dict(contents.get('inventory'))
        else:
            self.inventory = Fisher.definitive().inventory
        if contents.get('feat_book'):
            self.feat_book = FeatBook.from_dict(contents.get('feat_book'))
        else:
            self.feat_book = Fisher.definitive().feat_book
        if contents.get('recipe_book'):
            self.recipe_book = RecipeBook.from_dict(contents.get('recipe_book'))
        else:
            self.recipe_book = Fisher.definitive().recipe_book
        if contents.get('time_tokens'):
            self.time_tokens = TimeTokens.from_dict(contents.get('time_tokens'))
        else:
            self.time_tokens = Fisher.definitive().time_tokens

        if contents.get('catch_queue'):
            self.catch_queue = CatchQueue.from_dict(contents.get('catch_queue'))
        else:
            self.catch_queue = Fisher.definitive().catch_queue

    def to_dict(self):
        """Return the fisher's savable keywords as a dict."""

        fisher_dict = {}
        for keyword in Fisher.savable_keywords:
            try:
                fisher_dict[keyword] = self.keyword_value(keyword).to_dict()
            except AttributeError:
                fisher_dict[keyword] = self.keyword_value(keyword)

        return fisher_dict

    def save_to_file(self, file):
        """Save the fisher's savable keywords to a file."""

        dump_object(self.to_dict(), file)

    @property
    def persistent_sea(self):
        return self.location_object.advance_and_get_dynamic_sea()

    @persistent_sea.setter
    def persistent_sea(self, dynamic_sea):
        self.location_object.dynamic_sea = dynamic_sea
