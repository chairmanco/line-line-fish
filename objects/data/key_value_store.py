from collections import OrderedDict


class KeyValueStore:

    def __init__(self, contents=None):
        self.contents = OrderedDict(contents.items) if contents else OrderedDict()

    def __len__(self):
        return len(self.contents)

    def set(self, key, value):
        self.contents[key] = value

    def set_with_condition(self, key, value, condition):
        if condition:
            self.contents[key] = value

    def get(self, key, default=None):
        return self.contents[key] if key in self.contents else default

    def get_with_default_key(self, key, default):
        return self.contents[key] if key in self.contents else self.contents[default]

    def items(self):
        return self.contents.items()

    def values(self):
        return self.contents.values()

    def keys(self):
        return self.contents.keys()

    def nth_key(self, index):
        try:
            return sorted(self.contents, key=lambda k: self.contents[k])[index]
        except IndexError:
            return None
