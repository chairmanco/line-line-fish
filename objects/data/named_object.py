class NamedObject:

    def __init__(self, name):
        self.name = name

    def count(self, substring):
        return self.name.count(substring)

    def contains(self, substring):
        return self.name.count(substring) > 0

    def __repr__(self):
        return self.name
