class FreqMod:
    """A modifier which can affect the frequency of a sea object in a sea."""

    def __init__(self, target_name, value):
        self.target_name = target_name
        self.value = value
