from objects.data.freq_mods.freq_mod import FreqMod


class AddItemFreq(FreqMod):
    """A modifier which can add to the frequency of an item catch object in a sea."""
