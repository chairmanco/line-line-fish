from objects.data.freq_mods.add_freq import AddFreq
from objects.data.freq_mods.add_item_freq import AddItemFreq
from objects.data.freq_mods.multiply_freq import MultiplyFreq


class FreqMods:

    def __init__(self, initializer=None):
        self.contents = []

        if initializer:
            for tup in initializer:
                if len(tup) == 3:
                    if tup[0] == 'multiply':
                        self.contents.append(MultiplyFreq(tup[1], tup[2]))
                    elif tup[0] == 'add':
                        self.contents.append(AddFreq(tup[1], tup[2]))
                    elif tup[0] == 'add item':
                        self.contents.append(AddItemFreq(tup[1], tup[2]))

    @staticmethod
    def definitive():
        return FreqMods(
            [('add', 'Anchovy', 0)]
        )

    def __len__(self):
        return len(self.contents)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return FreqMods(contents.get('freq_mods_list'))

    def to_dict(self):
        """Return this object as a dict."""

        freq_mods_list = []

        for freq_mod in self.contents:
            if isinstance(freq_mod, AddFreq):
                freq_mods_list.append(('add', freq_mod.target_name, freq_mod.value))
            elif isinstance(freq_mod, MultiplyFreq):
                freq_mods_list.append(('multiply', freq_mod.target_name, freq_mod.value))
            elif isinstance(freq_mod, AddItemFreq):
                freq_mods_list.append(('add item', freq_mod.target_name, freq_mod.value))
            else:
                freq_mods_list.append(('add', freq_mod.target_name, freq_mod.value))

        return {'freq_mods_list': freq_mods_list}
