from objects.data.freq_mods.freq_mod import FreqMod


class MultiplyFreq(FreqMod):
    """A modifier which can multiply the frequency of a sea object in a sea."""
