from objects.data.freq_mods.freq_mod import FreqMod


class AddFreq(FreqMod):
    """A modifier which can add to the frequency of a sea object in a sea."""
