from objects.data.frequency_store import FrequencyStore
from objects.data.location.location import Location


class Locations(FrequencyStore):
    """A collection of locations."""

    def __init__(self, initializer=None):
        FrequencyStore.__init__(self)
        if initializer:
            for name, location in initializer.items():
                self.contents[name] = location

    @staticmethod
    def definitive():
        """Return the definitive list of weathers."""

        return Locations(
            {
                'Safe Harbour': Location.definitive(),
                'Default': Location.definitive()
            }
        )
