from objects.data.freq_mods.freq_mods import FreqMods
from objects.data.frequency import Frequency


class Weather(Frequency):
    """A weather phenomenon which affect catch frequencies."""

    def __init__(self, name, freq, reduction_power, freq_mods):
        self.reduction_power = reduction_power
        self.freq_mods = freq_mods

        Frequency.__init__(self, name, freq)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return Weather(
            contents.get('name'),
            contents.get('freq'),
            contents.get('reduction_power'),
            FreqMods.from_dict(contents.get('freq_mods'))
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'freq': self.freq,
            'reduction_power': self.reduction_power,
            'freq_mods': self.freq_mods.to_dict()
        }
