from objects.data.freq_mods.freq_mods import FreqMods
from objects.data.frequency_store import FrequencyStore
from objects.data.location.weather import Weather


class Weathers(FrequencyStore):
    """A collection of weathers."""

    def __init__(self, initializer=None):
        FrequencyStore.__init__(self)
        if initializer:
            for name, inner in initializer.items():
                self.add_weather(name, inner['frequency'], inner['reduction_power'],
                                 inner['freq_mods'])

    @staticmethod
    def from_names(weather_name_freqs):
        """Return a list of weathers constructed from the definitive list via names and freqs."""

        weathers = Weathers()
        for name, freq in weather_name_freqs.items():
            weathers.add_weather_by_name(name, freq)
        return weathers

    @staticmethod
    def definitive():
        """Return the definitive list of weathers."""

        return Weathers(
            {
                'Fog': {
                    'frequency': 1,
                    'reduction_power': 0,
                    'freq_mods': [
                        ('multiply', 'Anchovy', 2),
                        ('multiply', 'Mermaid', 3)
                    ]
                },
                'Light Rain': {
                    'frequency': 1,
                    'reduction_power': 0,
                    'freq_mods': [
                        ('multiply', 'Salmon', 3),
                        ('multiply', 'Bluefin Tuna', 3),
                        ('multiply', 'Yellowfin Tuna', 3),
                    ]
                },
                'Heavy Rain': {
                    'frequency': 1,
                    'reduction_power': 0,
                    'freq_mods': [
                        ('multiply', 'Bull Shark', 3),
                        ('multiply', 'Marlin', 3)
                    ]
                }
            }
        )

    def add_weather(self, name, freq, reduction_power, freq_mods):
        """Add a weather from a dict."""

        self.contents[name] = Weather(name, freq, reduction_power, FreqMods(freq_mods))

    def add_weather_by_name(self, name, freq):
        """Add a weather from the definitive list via its name."""

        weather = Weathers.definitive().get(name)
        if weather:
            self.contents[name] = Weather(name, freq, weather.reduction_power, weather.freq_mods)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        weathers = Weathers()

        for key, value in contents.items():
            weathers.contents[key] = Weather.from_dict(value)

        return weathers

    def to_dict(self):
        """Return this object as a dict."""

        content_dict = {}

        for key, value in self.contents.items():
            content_dict[key] = value.to_dict()

        return content_dict
