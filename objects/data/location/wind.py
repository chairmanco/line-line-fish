class Wind:
    """A wind with random but constrained strengths."""

    def __init__(self, minimum, maximum, average, wind=-1):
        self.minimum = minimum
        self.maximum = maximum
        self.average = average
        if wind == -1:
            self.wind = average
        else:
            self.wind = wind

    def roll_wind(self):
        """Set the wind to a new value based on its constraints."""

        # TODO: Keep within min and max and weight towards average, but be random!
        # The wind will shift a bit each roll - big jumps are very rare!
        self.wind = self.wind

    def __repr__(self):
        return str(self.wind)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return Wind(
            contents.get('minimum'),
            contents.get('maximum'),
            contents.get('average'),
            contents.get('wind')
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'minimum': self.minimum,
            'maximum': self.maximum,
            'average': self.average,
            'wind': self.wind
        }
