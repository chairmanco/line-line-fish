import math
import random
import time

from functions.state.serialization import dump_object
from objects.data.freq_mods.freq_mods import FreqMods
from objects.data.location.weather import Weather
from objects.data.location.weathers import Weathers
from objects.data.location.wind import Wind
from objects.data.named_object import NamedObject
from objects.data.sea_objects.item_catch import ItemCatch
from objects.data.sea_objects.sea_object_type import SeaObjectType
from objects.data.sea_objects.simple_catch import SimpleCatch
from objects.data.seas.dynamic_sea import DynamicSea


class Location(NamedObject):
    """A location which can be visited by multiple fishers."""

    REDUCTION_INTERVAL = 60 * 60 * 1
    ADD_INTERVAL = 60 * 60 * 6

    savable_keywords = ['weather', 'weather_remaining', 'weather_chances', 'wind',
                        'last_add', 'last_reduction', 'dynamic_sea_additions',
                        'static_sea_modifiers', 'dynamic_sea',
                        'fisher_units', 'local_units',
                        'fisher_list', 'leader_board', 'fisher_messages']

    def __init__(self, name, contents=None):
        self.weather = None
        self.weather_remaining = None
        self.weather_chances = None
        self.wind = None

        self.last_add = None
        self.last_reduction = None
        self.dynamic_sea_additions = None

        self.static_sea_modifiers = None
        self.dynamic_sea = None

        self.fisher_units = None
        self.local_units = None

        self.fisher_list = None
        self.leader_board = None
        self.fisher_messages = None

        NamedObject.__init__(self, name)

        if contents:
            self.load_from_dict(contents)

    def init_location(self, contents):
        """Initialize the location with actual contents."""

        for keyword in Location.savable_keywords:
            try:
                self.__dict__[keyword] = (
                        contents.get(keyword) or Location.definitive().keyword_value(keyword)
                )
            except (RuntimeError, ValueError, AttributeError, TypeError, IndexError):
                self.__dict__[keyword] = Location.definitive().keyword_value(keyword)

    def keyword_value(self, keyword):
        """Return the location's value for a keyword."""

        return self.__dict__[keyword]

    @staticmethod
    def definitive():
        """Return the standard starting Location."""

        default_location = Location('Safe Harbour')

        default_location.weather = Weathers.definitive().get('Fog')
        default_location.weather_remaining = 15
        default_location.weather_chances = Weathers.from_names(
            {
                'Fog': 50,
                'Light Rain': 20,
                'Heavy Rain': 10
            }
        )
        default_location.wind = Wind(0, 10, 5)

        default_location.last_add = None
        default_location.last_reduction = None
        default_location.dynamic_sea_additions = {
            'SingleTuna': {'type': SeaObjectType.SIMPLE, 'frequency': 1},
            'Driftwood': {'type': SeaObjectType.ITEM, 'frequency': 500},
            'Seaweed': {'type': SeaObjectType.ITEM, 'frequency': 500},
            'Scrap Metal': {'type': SeaObjectType.ITEM, 'frequency': 500},
            'Glass Bottle': {'type': SeaObjectType.ITEM, 'frequency': 500},
            'Giant Clam': {'type': SeaObjectType.ITEM, 'frequency': 500},
            'Default': {'type': SeaObjectType.SIMPLE, 'frequency': 0}
        }

        default_location.static_sea_modifiers = FreqMods(
            [
                ('add item', 'Driftwood', 10),
                ('add item', 'Seaweed', 10),
                ('add item', 'Scrap Metal', 10),
                ('add item', 'Glass Bottle', 10),
                ('add item', 'Giant Clam', 10)
            ]
        )
        default_location.dynamic_sea = DynamicSea(
            default_location.dynamic_sea_additions
        )

        default_location.fisher_units = None
        default_location.local_units = None

        default_location.fisher_list = None
        default_location.leader_board = None
        default_location.fisher_messages = None

        return default_location

    def advance_weather(self):
        """Get and advance the current weather, rolling a new weather if needed."""

        self.weather_remaining -= 1

        if self.weather_remaining <= 0:
            self.weather = self.weather_chances.get_random_value()
            self.weather_remaining = random.randrange(15, 151)

        return self.weather

    def load_from_dict(self, contents):
        """Initialize the fisher with a contents dict."""

        for keyword in Location.savable_keywords:
                self.__dict__[keyword] = (
                        contents.get(keyword) or Location.definitive().keyword_value(keyword)
                )

        if contents.get('weather'):
            self.weather = Weather.from_dict(contents.get('weather'))
        else:
            self.weather = Location.definitive().weather
        if contents.get('weather_chances'):
            self.weather_chances = Weathers.from_dict(contents.get('weather_chances'))
        else:
            self.weather_chances = Location.definitive().weather_chances
        if contents.get('wind'):
            self.wind = Wind.from_dict(contents.get('wind'))
        else:
            self.wind = Location.definitive().wind

        if contents.get('static_sea_modifiers'):
            self.static_sea_modifiers = FreqMods.from_dict(contents.get('static_sea_modifiers'))
        else:
            self.static_sea_modifiers = Location.definitive().static_sea_modifiers
        if contents.get('dynamic_sea'):
            self.dynamic_sea = DynamicSea.from_dict(contents.get('dynamic_sea'))
        else:
            self.dynamic_sea = Location.definitive().dynamic_sea

    def advance_and_get_dynamic_sea(self):
        """Return the location's dynamic sea after applying effects due to the passing of time."""

        current_time = math.floor(time.time())

        adds = 0
        reductions = 0

        if self.last_add:
            if current_time - self.last_add >= Location.ADD_INTERVAL:
                adds = math.floor((current_time - self.last_add) / Location.ADD_INTERVAL)
                self.last_add = current_time
        else:
            self.last_add = current_time

        if self.last_reduction:
            if current_time - self.last_reduction >= Location.REDUCTION_INTERVAL:
                reductions = math.floor(
                    (current_time - self.last_reduction) / Location.REDUCTION_INTERVAL
                )
                self.last_reduction = current_time
        else:
            self.last_reduction = current_time

        reductions_per_add = Location.ADD_INTERVAL / Location.REDUCTION_INTERVAL

        while adds > 0:
            adds -= 1
            self.replenish_sea()
            reduction_limit = reductions_per_add
            while reduction_limit > 0 and reductions > 0:
                reduction_limit -= 1
                reductions -= 1
                self.decay_sea()

        while reductions > 0:
            reductions -= 1
            self.decay_sea()

        return self.dynamic_sea

    def replenish_sea(self):
        """Replenish the location's dynamic sea."""

        for name, inner in self.dynamic_sea_additions.items():
            self.dynamic_sea.add_from_dict(name, inner)

    def decay_sea(self):
        """Reduce by 10% the simple catches and non-sunken items in the location's dynamic sea."""

        for name, sea_item in list(self.dynamic_sea.items()):
            if type(sea_item) == SimpleCatch or type(sea_item) == ItemCatch:
                reduction = math.floor(sea_item.freq / 10)
                self.dynamic_sea.decrement_sea_object(name, reduction)

    def to_dict(self):
        """Return the location's savable keywords as a dict."""

        location_dict = {}
        for keyword in Location.savable_keywords:
            try:
                location_dict[keyword] = self.keyword_value(keyword).to_dict()
            except AttributeError:
                location_dict[keyword] = self.keyword_value(keyword)

        return location_dict

    def save_to_file(self, file):
        """Save the location's savable keywords to a file."""

        dump_object(self.to_dict(), file)
