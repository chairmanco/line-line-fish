from objects.data.fish_data.catch_data import CatchData


class Fish(CatchData):

    def __init__(self, name, weight, value, freq):
        CatchData.__init__(self, name, weight, value, freq)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return Fish(
            contents.get('name'),
            contents.get('weight'),
            contents.get('value'),
            contents.get('freq')
        )
