from objects.data.fish_data.fish import Fish
from objects.data.frequency_store import FrequencyStore


class Fishes(FrequencyStore):

    def __init__(self, initializer=None):
        FrequencyStore.__init__(self)
        if initializer:
            for name, inner in initializer.items():
                self.add_fish(name, inner['weight'], inner['value'], inner['frequency'])

    @staticmethod
    def definitive():
        return Fishes(
            {
                'Anchovy': {'weight': 1, 'value': 10, 'frequency': 2000},
                'Bluefin Tuna': {'weight': 20, 'value': 5, 'frequency': 500},
                'Yellowfin Tuna': {'weight': 25, 'value': 4, 'frequency': 500},
                'Salmon': {'weight': 15, 'value': 10, 'frequency': 250},
                'Marlin': {'weight': 200, 'value': 6, 'frequency': 10},
                'Whale Shark': {'weight': 1000, 'value': 0.1, 'frequency': 1},
                'Oarfish': {'weight': 400, 'value': 10, 'frequency': 5},
                'Bull Shark': {'weight': 300, 'value': 0, 'frequency': 10},
                'Great White Shark': {'weight': 600, 'value': 0, 'frequency': 1},
                'Spiny Lobster': {'weight': 20, 'value': 25, 'frequency': 0},
                'American Lobster': {'weight': 40, 'value': 20, 'frequency': 0},
                'Treasure Chest': {'weight': 100, 'value': 50, 'frequency': 0},
                'Legendary Relic': {'weight': 5, 'value': 20, 'frequency': 0},
                'Mermaid': {'weight': 75, 'value': 10, 'frequency': 0},
                'Lucky Boot': {'weight': 5, 'value': 4, 'frequency': 0},
                'Bad Omen Albatross': {'weight': 15, 'value': 5, 'frequency': 0},
                'Pirate\'s Discount Matchlock': {'weight': 10, 'value': 25, 'frequency': 0},
                'Thieving Octopus': {'weight': 20, 'value': 5, 'frequency': 0},
                'Kraken of Grand Larceny': {'weight': 900, 'value': 0, 'frequency': 0},
                'Endangered Sea Turtle': {'weight': 500, 'value': 5, 'frequency': 0},
                'No-Longer-Endangered Sea Turtle': {'weight': 500, 'value': 5, 'frequency': 0},
                'SingleTuna': {'weight': 111, 'value': 11, 'frequency': 0},
                'Lion\'s Mane Jelly': {'weight': 600, 'value': 0.2, 'frequency': 0},
                'Sea Snake': {'weight': 10, 'value': 10, 'frequency': 0},
                'Brittle Star': {'weight': 2, 'value': 75, 'frequency': 0},
                'Sunken Item': {'weight': 5, 'value': 0, 'frequency': 0},
                'Shark\'thulu': {'weight': 1100, 'value': 0, 'frequency': 0},
                'Default': {'weight': 10, 'value': 5, 'frequency': 0}
            }
        )

    def add_fish(self, name, weight, value, freq):
        self.contents[name] = Fish(name, weight, value, freq)
