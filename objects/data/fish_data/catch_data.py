from objects.data.frequency import Frequency


class CatchData(Frequency):

    def __init__(self, name, weight, value, freq):
        self.weight = weight
        self.value = value
        Frequency.__init__(self, name, freq)

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'weight': self.weight,
            'value': self.value,
            'freq': self.freq
        }
