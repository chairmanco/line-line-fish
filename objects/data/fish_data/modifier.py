from objects.data.fish_data.catch_data import CatchData


class Modifier(CatchData):

    def __init__(self, name, weight, value, freq):
        CatchData.__init__(self, name, weight, value, freq)

    def combine(self, other):
        """Combine with another modifier."""

        self.name = f'{self} {other}'.strip()
        self.weight *= other.weight
        self.value *= other.value
        self.freq += other.freq  # TODO: Do the actual stats reduction

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return Modifier(
            contents.get('name'),
            contents.get('weight'),
            contents.get('value'),
            contents.get('freq')
        )
