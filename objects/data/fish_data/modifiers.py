from objects.data.fish_data.modifier import Modifier
from objects.data.frequency_store import FrequencyStore


class Modifiers(FrequencyStore):

    def __init__(self, initializer=None):
        FrequencyStore.__init__(self)
        if initializer:
            for name, inner in initializer.items():
                self.add_modifier(name, inner['weight'], inner['value'], inner['frequency'])

    @staticmethod
    def definitive():
        return Modifiers(
            {
                'Common': {'weight': 1, 'value': 1, 'frequency': 100},
                'Juvenile': {'weight': 0.75, 'value': 1, 'frequency': 25},
                'Ancient': {'weight': 0.9, 'value': 1.25, 'frequency': 15},
                'Hearty': {'weight': 1.25, 'value': 1, 'frequency': 15},
                'Diseased': {'weight': 0.8, 'value': 0.6, 'frequency': 10},
                'Dark': {'weight': 1, 'value': 1.2, 'frequency': 7},
                'Albino': {'weight': 1, 'value': 1.4, 'frequency': 5},
                'Frenzied': {'weight': 1.1, 'value': 1, 'frequency': 5},
                'Newborn': {'weight': 0.1, 'value': 2.0, 'frequency': 3},
                'Gilded': {'weight': 1, 'value': 1.6, 'frequency': 3},
                'Iridescent': {'weight': 1, 'value': 2.0, 'frequency': 1},
                'Gut-Loaded': {'weight': 1.2, 'value': 0.9, 'frequency': 2},
                'Wise': {'weight': 1.2, 'value': 0.9, 'frequency': 2},
                'Luck-Bearing': {'weight': 1, 'value': 1.2, 'frequency': 1},
                'Omen-Bearing': {'weight': 1.1, 'value': 1.2, 'frequency': 1},
                'Discount-Earning': {'weight': 0.9, 'value': 1.2, 'frequency': 1},
                'Legendary': {'weight': 1.5, 'value': 1.5, 'frequency': 0}
            }
        )

    def add_modifier(self, name, weight, value, freq):
        self.contents[name] = Modifier(name, weight, value, freq)
