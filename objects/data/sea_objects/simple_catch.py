from objects.data.sea_objects.sea_object import SeaObject
from objects.data.sea_objects.sea_object_type import SeaObjectType


class SimpleCatch(SeaObject):
    """A fish which can be fished from a sea."""

    def __init__(self, name, quantity):
        self.fish_name = name
        SeaObject.__init__(self, name, quantity)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return SimpleCatch(
            contents.get('name'),
            contents.get('freq')
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'freq': self.freq,
            'object_type': SeaObjectType.SIMPLE
        }
