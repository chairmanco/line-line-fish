from objects.data.fish_data.catch_data import CatchData
from objects.data.fish_data.fish import Fish
from objects.data.fish_data.modifier import Modifier
from objects.data.sea_objects.sea_object import SeaObject
from objects.data.sea_objects.sea_object_type import SeaObjectType


class FullCatch(CatchData, SeaObject):
    """A fish with modifiers and stats which can be fished from a sea."""

    def __init__(self, name, weight, value, freq, base_fish, modifiers):
        self.base_fish = base_fish
        self.modifiers = modifiers
        self.captured = False
        CatchData.__init__(self, name, weight, value, freq)
        SeaObject.__init__(self, name, freq)

    def parent_species(self):
        """Return the species of this catch."""

        return self.base_fish.name

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        modifier_list = []
        for modifier in contents.get('modifiers'):
            modifier_list.append(Modifier.from_dict(modifier))

        return FullCatch(
            contents.get('name'),
            contents.get('weight'),
            contents.get('value'),
            contents.get('freq'),
            Fish.from_dict(contents.get('base_fish')),
            modifier_list
        )

    def to_dict(self):
        """Return this object as a dict."""

        modifier_dict_list = []
        for modifier in self.modifiers:
            modifier_dict_list.append(modifier.to_dict())

        return {
            'name': self.name,
            'weight': self.weight,
            'value': self.value,
            'freq': self.freq,
            'base_fish': self.base_fish.to_dict(),
            'modifiers': modifier_dict_list,
            'object_type': SeaObjectType.FULL
        }
