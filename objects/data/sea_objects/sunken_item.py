from objects.data.items.inventory_item_factory import InventoryItemFactory
from objects.data.sea_objects.item_catch import ItemCatch
from objects.data.sea_objects.sea_object_type import SeaObjectType


class SunkenItem(ItemCatch):
    """An item which can be fished from a sea."""

    def __init__(self, name, qty, item):
        self.item = item
        ItemCatch.__init__(self, name, qty)

    def get_item(self):
        """Get the item stored within this SunkenItem."""

        return self.item

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return SunkenItem(
            contents.get('name'),
            contents.get('qty'),
            InventoryItemFactory.make_from_dict(contents.get('item'))
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'weight': self.weight,
            'value': self.value,
            'qty': self.freq,
            'item': self.item.to_dict(),
            'object_type': SeaObjectType.SUNKEN_ITEM
        }
