from enum import IntEnum


class SeaObjectType(IntEnum):
    NO_TYPE = 0
    SIMPLE = 1
    MODIFIED = 2
    FULL = 3
    ITEM = 4
    SUNKEN_ITEM = 5
    CHECKABLE_UNIT = 6
