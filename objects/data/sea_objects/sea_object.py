from objects.data.frequency import Frequency
from objects.data.sea_objects.sea_object_type import SeaObjectType


class SeaObject(Frequency):
    """An object which can be fished from a sea."""

    def __init__(self, name, quantity):
        Frequency.__init__(self, name, quantity)

    def __repr__(self):
        return self.name

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return SeaObject(
            contents.get('name'),
            contents.get('freq')
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'freq': self.freq,
            'object_type': SeaObjectType.NO_TYPE
        }
