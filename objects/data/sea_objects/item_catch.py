from objects.data.fish_data.catch_data import CatchData
from objects.data.fish_data.fishes import Fishes
from objects.data.items.inventory import Inventory
from objects.data.sea_objects.sea_object import SeaObject
from objects.data.sea_objects.sea_object_type import SeaObjectType


class ItemCatch(SeaObject, CatchData):
    """An item which can be fished from a sea."""

    def __init__(self, name, qty):
        CatchData.__init__(
            self,
            name,
            Fishes.definitive().get_with_default_key('Sunken Item', 'Default').weight,
            Fishes.definitive().get_with_default_key('Sunken Item', 'Default').value,
            qty
        )
        SeaObject.__init__(self, name, qty)

    def get_item(self):
        """Get the item associated with this ItemCatch's name."""

        return Inventory.definitive().get(self.name)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return ItemCatch(
            contents.get('name'),
            contents.get('qty')
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'weight': self.weight,
            'value': self.value,
            'qty': self.freq,
            'object_type': SeaObjectType.ITEM
        }
