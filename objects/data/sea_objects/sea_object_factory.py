from objects.data.sea_objects.sea_object_type import SeaObjectType


class SeaObjectFactory:
    """Makes sea objects from dictionaries."""

    @staticmethod
    def make_from_dict(contents):
        """Make a sea object from a dict."""

        from objects.data.sea_objects.full_catch import FullCatch
        from objects.data.sea_objects.item_catch import ItemCatch
        from objects.data.sea_objects.modified_catch import ModifiedCatch
        from objects.data.sea_objects.sea_object import SeaObject
        from objects.data.sea_objects.simple_catch import SimpleCatch
        from objects.data.sea_objects.sunken_item import SunkenItem

        object_type = contents.get('object_type')

        if object_type == SeaObjectType.NO_TYPE:
            return SeaObject.from_dict(contents)

        if object_type == SeaObjectType.SIMPLE:
            return SimpleCatch.from_dict(contents)

        if object_type == SeaObjectType.MODIFIED:
            return ModifiedCatch.from_dict(contents)

        if object_type == SeaObjectType.FULL:
            return FullCatch.from_dict(contents)

        if object_type == SeaObjectType.ITEM:
            return ItemCatch.from_dict(contents)

        if object_type == SeaObjectType.SUNKEN_ITEM:
            return SunkenItem.from_dict(contents)

        return SeaObject.from_dict(contents)
