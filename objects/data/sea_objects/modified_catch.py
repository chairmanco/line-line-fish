from objects.data.fish_data.modifiers import Modifiers
from objects.data.sea_objects.sea_object import SeaObject
from objects.data.sea_objects.sea_object_type import SeaObjectType


class ModifiedCatch(SeaObject):
    """A fish with modifier_list which can be fished from a sea."""

    def __init__(self, name, quantity, fish_name, modifier_names):
        self.fish_name = fish_name
        self.modifier_names = modifier_names
        if not name:
            name = self.generate_name()
        SeaObject.__init__(self, name, quantity)

    def add_modifier(self, modifier):
        """Add another modifier to this catch."""

        self.modifier_names.append(modifier)
        self.generate_name()

    def generate_name(self):
        """Generate, set, and return the combination of this catch's fish and modifier names."""

        try:
            generated_name = self.modifier_names[0]

            if len(self.modifier_names) > 1:
                for modifier in self.modifier_names[1:]:
                    generated_name = f'{generated_name} {modifier}'

            generated_name = f'{generated_name} {self.fish_name}'
        except IndexError:
            generated_name = self.fish_name

        self.name = generated_name
        return generated_name

    def extract_modifier(self):
        """Create a modifier object using this catch's modifier names."""

        try:
            first_modifier = Modifiers.definitive().get(self.modifier_names[0])

            if len(self.modifier_names) > 1:
                for modifier in self.modifier_names[1:]:
                    new_modifier = Modifiers.definitive().get(modifier)
                    first_modifier.combine(new_modifier)

            return first_modifier
        except IndexError:
            return None

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return ModifiedCatch(
            contents.get('name'),
            contents.get('freq'),
            contents.get('fish_name'),
            contents.get('modifier_names')
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'freq': self.freq,
            'fish_name': self.fish_name,
            'modifier_names': self.modifier_names,
            'object_type': SeaObjectType.MODIFIED
        }
