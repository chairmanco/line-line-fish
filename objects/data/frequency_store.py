import random

from objects.data.frequency import Frequency
from objects.data.key_value_store import KeyValueStore


class FrequencyStore(KeyValueStore):

    def __init__(self, initializer=None):
        KeyValueStore.__init__(self)
        if initializer:
            for name, inner in initializer.items():
                self.add_frequency(name, inner['frequency'])

    def add_frequency(self, name, freq):
        """Add a frequency."""

        self.contents[name] = Frequency(name, freq)

    def increase_frequency(self, name, freq):
        """Increase a frequency, adding it if absent."""

        if self.contents.get(name):
            self.contents[name].freq += freq
        else:
            self.contents[name] = Frequency(name, freq)

    def decrease_frequency(self, name, freq=1):
        """Decrease a frequency and remove it if zero or negative."""

        if self.contents.get(name):
            if freq >= self.contents[name].freq:
                del self.contents[name]
            else:
                self.contents[name].freq -= freq

    def remove_frequency(self, name):
        """Remove a frequency."""

        if self.get(name):
            del self.contents[name]

    def get_random(self):
        """Get a random key and value pair from this FrequencyStore."""

        if len(self.contents) <= 0:
            self.contents = {'SingleTuna': Frequency('SingleTuna', 1)}

        items = []
        freqs = []
        lower_limit = []
        upper_limit = []
        sum_freq = 0

        # trying to emulate numpy.choice without having to add a dependency to 360cli
        # numpy.choice(items, p=[f / total for f in freqs])

        if not len(self.contents.items()):
            return None

        for key, frequency in self.contents.items():
            items.append((key, frequency))
            freqs.append(frequency.freq)
            lower_limit.append(sum_freq)
            sum_freq += frequency.freq
            upper_limit.append(sum_freq)

        choice = random.randint(0, sum_freq - 1)

        for index in range(len(items)):
            if lower_limit[index] <= choice < upper_limit[index]:
                return items[index]

    def get_random_value(self):
        """Get a random value from this FrequencyStore."""

        return self.get_random()[1]

    def get_random_key(self):
        """Get a random key from this FrequencyStore."""

        return self.get_random()[0]

    def get_total(self):
        """Get the total of the frequencies in this FrequencyStore"""

        total = 0
        for frequency in self.contents.values():
            total += frequency.freq

        return total
