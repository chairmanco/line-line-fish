from objects.data.sea_objects.sea_object_factory import SeaObjectFactory
from objects.data.seas.sea import Sea
from objects.data.sea_objects.sea_object_type import SeaObjectType
from objects.data.sea_objects.sunken_item import SunkenItem


class DynamicSea(Sea):
    """A collection of sea objects whose items represent quantities that decrease when caught."""

    @staticmethod
    def definitive():
        return DynamicSea(
            {
                'SingleTuna': {'type': SeaObjectType.SIMPLE, 'frequency': 1}
            }
        )

    def fish_from_sea(self):
        """Return a SeaObject from this sea and decrement its frequency."""

        key, value = self.get_random()
        self.decrease_frequency(key)
        if len(self) == 0:
            self.contents = DynamicSea.definitive().contents

        return value

    def add_sunken_item(self, item, qty=-1):
        """Add a sunken item to this sea."""

        name = f'Sunken Item <{item.name}>'

        if name in self.keys():
            name = self.make_name_unique(name)
        if qty == -1:
            qty = item.qty

        self.contents[name] = SunkenItem(item.name, qty, item)

    def make_name_unique(self, name):
        """Append a name with a number to ensure uniqueness."""

        num = 0
        unique = f'{name} {num}'
        while unique in self.keys():
            num += 1
            unique = f'{name} {num}'

        return unique

    def remove_sea_object(self, name):
        del self.contents[name]

    def decrement_sea_object(self, name, qty=1):
        if name in self.contents.keys():
            self.get(name).freq -= qty
            if self.get(name).freq <= 0:
                self.remove_sea_object(name)

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        sea = DynamicSea()
        content_dict = contents.get('contents')

        if content_dict:
            for key, value in content_dict.items():
                sea.contents[key] = SeaObjectFactory.make_from_dict(value)

        return sea
