from objects.data.freq_mods.add_freq import AddFreq
from objects.data.freq_mods.add_item_freq import AddItemFreq
from objects.data.freq_mods.freq_mods import FreqMods
from objects.data.freq_mods.multiply_freq import MultiplyFreq
from objects.data.sea_objects.item_catch import ItemCatch
from objects.data.sea_objects.sea_object_factory import SeaObjectFactory
from objects.data.seas.sea import Sea
from objects.data.sea_objects.sea_object_type import SeaObjectType
from objects.data.sea_objects.simple_catch import SimpleCatch


class StaticSea(Sea):
    """A collection of sea objects whose items are meant to be caught indefinitely."""

    @staticmethod
    def definitive():
        return StaticSea(
            {
                'Treasure Chest': {'type': SeaObjectType.SIMPLE, 'frequency': 1},
                'Legendary Relic': {'type': SeaObjectType.SIMPLE, 'frequency': 1},
                'Mermaid': {'type': SeaObjectType.SIMPLE, 'frequency': 10},
                'Lucky Boot': {'type': SeaObjectType.SIMPLE, 'frequency': 25},
                'Bad Omen Albatross': {'type': SeaObjectType.SIMPLE, 'frequency': 20},
                'Pirate\'s Discount Matchlock': {'type': SeaObjectType.SIMPLE, 'frequency': 25},
                'Thieving Octopus': {'type': SeaObjectType.SIMPLE, 'frequency': 20},
                'Endangered Sea Turtle': {'type': SeaObjectType.SIMPLE, 'frequency': 15},
                'Shark\'thulu': {'type': SeaObjectType.SIMPLE, 'frequency': 1},
                'Kraken of Grand Larceny': {'type': SeaObjectType.SIMPLE, 'frequency': 2},
                'Default': {'type': SeaObjectType.SIMPLE, 'frequency': 0}
            }
        )

    def apply_freq_mods(self, freq_mods):
        """Apply frequency modifiers."""

        if isinstance(freq_mods, FreqMods):
            for freq_mod in freq_mods.contents:
                self.apply_freq_mod(freq_mod)

    def apply_freq_mod(self, freq_mod):
        """Apply a frequency modifier."""

        if isinstance(freq_mod, AddFreq):
            if freq_mod.target_name in self.keys():
                self.contents[freq_mod.target_name].freq += freq_mod.value
                if self.contents[freq_mod.target_name].freq <= 0:
                    del self.contents[freq_mod.target_name]
            else:
                if freq_mod.value > 0:
                    self.contents[freq_mod.target_name] = SimpleCatch(
                        freq_mod.target_name, freq_mod.value
                    )

        if isinstance(freq_mod, AddItemFreq):
            if freq_mod.target_name in self.keys():
                self.contents[freq_mod.target_name].freq += freq_mod.value
                if self.contents[freq_mod.target_name].freq <= 0:
                    del self.contents[freq_mod.target_name]
            else:
                if freq_mod.value > 0:
                    self.contents[freq_mod.target_name] = ItemCatch(
                        freq_mod.target_name, freq_mod.value
                    )

        if isinstance(freq_mod, MultiplyFreq):
            if freq_mod.target_name in self.keys():

                self.contents[freq_mod.target_name].freq = round(
                    self.contents[freq_mod.target_name].freq * freq_mod.value
                )
                if self.contents[freq_mod.target_name].freq <= 0:
                    del self.contents[freq_mod.target_name]

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        sea = StaticSea()
        content_dict = contents.get('contents')

        if content_dict:
            for key, value in content_dict.items():
                sea.contents[key] = SeaObjectFactory.make_from_dict(value)

        return sea
