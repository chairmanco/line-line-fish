from objects.data.frequency_store import FrequencyStore
from objects.data.key_value_store import KeyValueStore
from objects.data.sea_objects.item_catch import ItemCatch
from objects.data.sea_objects.sea_object_type import SeaObjectType
from objects.data.sea_objects.simple_catch import SimpleCatch


class Sea(FrequencyStore):
    """A collection of sea objects which can be caught."""

    def __init__(self, initializer=None):
        KeyValueStore.__init__(self)
        if initializer:
            for name, inner in initializer.items():
                self.add_from_dict(name, inner)

    def add_from_dict(self, name, dictionary):
        """Add a sea object from a dictionary."""

        if dictionary['type'] == SeaObjectType.SIMPLE:
            if self.contents.get(name):
                self.contents[name].freq += dictionary['frequency']
            else:
                self.contents[name] = SimpleCatch(name, dictionary['frequency'])

        if dictionary['type'] == SeaObjectType.ITEM:
            if self.contents.get(name):
                self.contents[name].freq += dictionary['frequency']
            else:
                self.contents[name] = ItemCatch(name, dictionary['frequency'])

    def add_simple(self, name, freq):
        """Add a simple catch to this sea."""

        self.contents[name] = SimpleCatch(name, freq)

    def increase_frequency(self, name, freq):
        """Increase a frequency, adding it if absent."""

        if self.contents.get(name):
            self.contents[name].freq += freq
        else:
            self.contents[name] = SimpleCatch(name, freq)

    def absorb(self, other):
        """Absorb another sea's frequencies."""

        if isinstance(other, Sea):
            for key, value in other.items():
                if key in self.keys():
                    self.contents[key].freq += value.freq
                else:
                    self.contents[key] = value

    def fish_from_sea(self):
        """Return a SeaObject from this sea."""

        return self.get_random_value()

    def to_dict(self):
        """Return this object as a dict."""

        content_dict = {}

        for key, value in self.contents.items():
            content_dict[key] = value.to_dict()

        return {
            'container_type': 'sea',
            'contents': content_dict
        }
