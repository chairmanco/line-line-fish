class VerySimpleQueue:

    def __init__(self):
        self.contents = []

    def empty(self):
        """Return true iff this queue is empty."""

        return len(self.contents) == 0

    def put(self, element):
        """Enqueue an element."""

        self.contents.append(element)

    def get(self):
        """De-queue an element."""

        if self.empty():
            return None
        else:
            next_element = self.contents[0]
            self.contents = self.contents[1:]
            return next_element
