import math
import time


class TimeTokens:
    """Stores tokens which accumulate in real-time and can be used to fish."""

    MAX_TOKENS = 60
    ADD_AMOUNT = 10
    INTERVAL = 60 * 10

    def __init__(self, tokens=50, last_add=None):
        self.tokens = tokens
        self.last_add = last_add

    def add_tokens(self):
        """Add time tokens if enough time has passed and return the new number of tokens."""

        current_time = math.floor(time.time())

        if self.last_add:
            if current_time - self.last_add >= TimeTokens.INTERVAL:
                self.tokens += TimeTokens.ADD_AMOUNT * math.floor(
                    (current_time - self.last_add) / TimeTokens.INTERVAL
                )
                self.last_add = current_time
                if self.tokens >= TimeTokens.MAX_TOKENS:
                    self.tokens = TimeTokens.MAX_TOKENS
        else:
            self.last_add = current_time

        return self.tokens

    def get_token(self):
        """Iff any, decrement the number of time tokens and return true."""

        if self.tokens > 0:
            self.tokens -= 1
            return True
        else:
            return False

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return TimeTokens(contents.get('tokens'), contents.get('last_add'))

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'tokens': self.tokens,
            'last_add': self.last_add
        }
