from objects.data.named_object import NamedObject


class Frequency(NamedObject):

    def __init__(self, name, freq):
        self.freq = freq
        NamedObject.__init__(self, name)
