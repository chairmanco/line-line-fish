from objects.info.item_type import ItemType


class InventoryItemFactory:
    """Makes inventory items from dictionaries."""

    @staticmethod
    def make_from_dict(contents):
        """Make a inventory item from a dict."""

        from objects.data.items.inventory_item import InventoryItem
        from objects.data.items.live_catch import LiveCatch

        use_type = contents.get('use_type')

        if use_type == ItemType.LIVE_CATCH:
            return LiveCatch.from_dict(contents)

        return InventoryItem.from_dict(contents)
