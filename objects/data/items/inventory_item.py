from objects.data.frequency import Frequency
from objects.info.item_type import ItemType


class InventoryItem(Frequency):

    def __init__(self, name, qty, data):
        self.use_type = ItemType.NO_TYPE
        self.buy = 0.0
        self.sell = 0.0
        self.text = ''
        self.meta = None
        self.weight = 0.0
        self.meta_type = name
        self.__dict__.update(data)
        Frequency.__init__(self, name, qty)

    def __lt__(self, other):
        if (self.use_type < other.use_type or
                (self.use_type == other.use_type and self.buy < other.buy)):
            return True

        return False

    @property
    def qty(self):
        return self.freq

    @qty.setter
    def qty(self, qty):
        self.freq = qty

    def get_sell_price(self):
        """Get this item's sell price."""

        return self.buy / 10 if self.sell == -1 else self.sell

    def clone(self):
        """Return a copy of this object."""

        return InventoryItem(
            self.name,
            self.qty,
            {
                'use_type': self.use_type, 'buy': self.sell, 'sell': self.sell,
                'text': self.text
            }
        )

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return InventoryItem(
            contents.get('name'),
            contents.get('qty'),
            {
                'use_type': contents.get('use_type'), 'buy': contents.get('buy'),
                'sell': contents.get('sell'), 'text': contents.get('text')
            }
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'qty': self.qty,
            'use_type': self.use_type,
            'buy': self.buy,
            'sell': self.sell,
            'text': self.text
        }
