from functions.text.conditional_text import cond_string
from objects.data.frequency_store import FrequencyStore
from objects.data.items.inventory_item import InventoryItem
from objects.data.items.inventory_item_factory import InventoryItemFactory
from objects.info.item_info import all_items


class Inventory(FrequencyStore):

    def __init__(self, initializer=None):
        FrequencyStore.__init__(self)
        if initializer:
            for name, inner in initializer.items():
                self.add_item_dict(name, inner)

    @staticmethod
    def definitive():
        return Inventory(
            all_items
        )

    @staticmethod
    def for_sale():
        store = Inventory()
        for name, inner in all_items.items():
            if inner['buy'] and inner.get('in_main_store'):
                Inventory.add_item_dict(store, name, inner)
        return store

    def add_item_raw(self, item):
        """Add an item without creating a new instance."""

        self.contents[item.name] = item

    def add_item(self, name, qty, data):
        self.contents[name] = (
            InventoryItem(
                name,
                self.get(name).qty + qty if name in self.contents.keys() else qty,
                data
            )
        )

    def add_item_dict(self, name, dictionary, qty=1):
        self.add_item(
            name,
            qty,
            {
                'use_type': dictionary['use_type'],
                'buy': dictionary['buy'],
                'sell': dictionary['sell'],
                'text': f'{dictionary["text"]}' + cond_string(f'\n\n\"{dictionary["flavour"]}\"',
                                                              dictionary['flavour'])
            }
        )

    def add_item_item(self, item, qty=1):
        self.add_item(
            item.name,
            qty,
            {
                'use_type': item.use_type,
                'buy': item.buy,
                'sell': item.sell,
                'text': item.text
            }
        )

    def remove_item(self, name):
        del self.contents[name]

    def decrement_item(self, name, qty=1):
        if name in self.contents.keys():
            self.get(name).qty -= qty
            if self.get(name).qty <= 0:
                self.remove_item(name)

    def find_item_containing(self, containing, not_containing=None):
        """Returns the first item in this inventory matching the arguments."""

        for key, item in self.contents.items():
            if not_containing:
                if containing in key and not_containing not in key:
                    return item
            else:
                if containing in key:
                    return item

        return None

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        inventory = Inventory()
        content_dict = contents.get('contents')

        if content_dict:
            for key, value in content_dict.items():
                inventory.contents[key] = InventoryItemFactory.make_from_dict(value)

        return inventory

    def to_dict(self):
        """Return this object as a dict."""

        content_dict = {}

        for key, value in self.contents.items():
            content_dict[key] = value.to_dict()

        return {
            'container_type': 'inventory',
            'contents': content_dict
        }
