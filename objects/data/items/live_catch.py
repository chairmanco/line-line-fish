from objects.data.items.inventory_item import InventoryItem
from objects.data.sea_objects.sea_object_factory import SeaObjectFactory
from objects.info.item_type import ItemType


class LiveCatch(InventoryItem):

    def __init__(self, name, qty, data, catch):
        self.catch = catch
        InventoryItem.__init__(self, name, qty, data)

    def clone(self):
        """Return a copy of this object."""

        return LiveCatch(
            self.name,
            self.qty,
            {
                'use_type': self.use_type, 'buy': self.sell, 'sell': self.sell,
                'text': self.text
            },
            self.catch
        )

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return LiveCatch(
            contents.get('name'),
            contents.get('qty'),
            {
                'use_type': contents.get('use_type'), 'buy': contents.get('buy'),
                'sell': contents.get('sell'), 'text': contents.get('text')
            },
            SeaObjectFactory.make_from_dict(contents.get('catch'))
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'qty': self.qty,
            'use_type': ItemType.LIVE_CATCH,
            'buy': self.buy,
            'sell': self.sell,
            'text': self.text,
            'catch': self.catch.to_dict()
        }
