import random

from functions.text.logging import log_text
from objects.info.recipe import recipe_list, Recipe


class RecipeBook:

    def __init__(self):
        self.excess_wisdom = 0  # TODO: Make this do something
        self.all_recipes = None
        self.all_recipes = self.get_all_recipes()
        self.recipe_list = [False] * len(self.get_all_recipes())

    def __len__(self):
        return len(self.all_recipes)

    def get_all_recipes(self, recipes=recipe_list):
        """Register recipes as Recipe objects."""

        if self.all_recipes:
            return self.all_recipes

        all_recipes_list = []

        for name, recipe in recipes.items():
            all_recipes_list.append(Recipe(name, recipe['ingredients'], recipe['products']))

        return all_recipes_list

    def toggle_recipe(self, recipe_num):
        """Toggle a recipe known, giving output if it was previously unknown."""

        if not self.recipe_list[recipe_num]:
            self.recipe_list[recipe_num] = True
            log_text(f'Learned recipe:\n{self.get_all_recipes()[recipe_num]}\n')

    def toggle_unknown_recipe(self):
        """Toggle an unknown recipe, or increase wisdom if all recipes are known."""

        unknown_indices = []

        for i in range(len(self)):
            if not self.recipe_list[i]:
                unknown_indices.append(i)

        if len(unknown_indices) == 0:
            self.excess_wisdom += 1
            log_text('You could not learn any more recipes, but you did grow wiser.\n')
            return False
        else:
            self.toggle_recipe(random.choice(unknown_indices))
            return True

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        recipe_book = RecipeBook()
        recipe_book.excess_wisdom = contents.get('excess_wisdom')
        recipe_book.recipe_list = contents.get('recipe_list')
        return recipe_book

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'excess_wisdom': self.excess_wisdom,
            'recipe_list': self.recipe_list
        }
