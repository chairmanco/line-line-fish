from enum import IntEnum


class ItemType(IntEnum):
    NO_TYPE = 0
    CONSUMABLE_CATCH_ALL = 1  # Single Use, Use All Next Catch
    CONSUMABLE_CATCH_ONE = 2  # Single Use, Use One Next Catch
    CONSUMABLE_AUTO = 3  # Single Use, Conditional
    PERSISTENT_MULTI = 4  # Persistent Stack-able
    PERSISTENT_SINGLE = 5  # Persistent
    PERSISTENT_RELIC = 6  # Legendary Relic
    LIVE_CATCH = 7  # Live Fish Catch
    CRAFTING = 8  # Crafting Ingredient
    BOAT = 9  # Fishing Boat
    ROD = 10  # Fishing Rod


def use_type_string(use_type):
    """Describe a use type in readable words."""

    if use_type == ItemType.CONSUMABLE_CATCH_ALL:
        return 'Single Use; use ALL next catch'
    if use_type == ItemType.CONSUMABLE_CATCH_ONE:
        return 'Single Use; use ONE next catch'
    if use_type == ItemType.CONSUMABLE_AUTO:
        return 'Single Use; used conditionally'
    if use_type == ItemType.PERSISTENT_MULTI:
        return 'Persistent; stack-able effect'
    if use_type == ItemType.PERSISTENT_SINGLE:
        return 'Persistent; unique effect'
    if use_type == ItemType.PERSISTENT_RELIC:
        return 'Persistent (Legendary Relic); unique effect'
    if use_type == ItemType.LIVE_CATCH:
        return 'Single Use; can be bred or sold'
    if use_type == ItemType.CRAFTING:
        return 'Single Use; crafting ingredient'
    if use_type == ItemType.BOAT:
        return 'Single Use; replaces current boat'
    if use_type == ItemType.ROD:
        return 'Single Use; replaces current rod'

    return 'N/A'
