from objects.data.named_object import NamedObject

recipe_list = {
    'Anti-Venom': {
        'ingredients': {
            'Venom': 1
        },
        'products': {
            'Anti-Venom': 10
        }
    },
    'Krakengift': {
        'ingredients': {
            'Venom': 3
        },
        'products': {
            'Krakengift': 15
        }
    },
    'DIY Shipbuilding Kit': {
        'ingredients': {
            'Driftwood': 1,
            'Seaweed': 1
        },
        'products': {
            'DIY Shipbuilding Kit': 1
        }
    },
    'Portable Aquarium': {
        'ingredients': {
            'Glass Bottle': 1,
            'Seaweed': 1
        },
        'products': {
            'Portable Aquarium': 1
        }
    },
    'Piscine Reducer': {
        'ingredients': {
            'Glass Bottle': 1,
            'Venom': 1,
            'Seaweed': 1
        },
        'products': {
            'Piscine Reducer': 1
        }
    },
    'Marine Creature Breeding Kit': {
        'ingredients': {
            'Giant Clam': 1,
            'Glass Bottle': 1
        },
        'products': {
            'Marine Creature Breeding Kit': 1
        }
    },
    'Admiral Burger Combo': {
        'ingredients': {
            'Giant Clam': 1,
            'Seaweed': 1
        },
        'products': {
            'Admiral Burger Combo': 50
        }
    },
    'Love Letter to the Lady of the Sea': {
        'ingredients': {
            'Driftwood': 2
        },
        'products': {
            'Love Letter to the Lady of the Sea': 100
        }
    },
    'Gutting Knife': {
        'ingredients': {
            'Scrap Metal': 1
        },
        'products': {
            'Gutting Knife': 1
        }
    },
    'Lobster Cage': {
        'ingredients': {
            'Scrap Metal': 2,
            'Driftwood': 1
        },
        'products': {
            'Lobster Cage': 1
        }
    },
    'Artificial Mermaid': {
        'ingredients': {
            'Scrap Metal': 1,
            'Seaweed': 1
        },
        'products': {
            'Artificial Mermaid': 1
        }
    },
    'Venom Skimmer': {
        'ingredients': {
            'Glass Bottle': 1,
            'Driftwood': 1
        },
        'products': {
            'Venom Skimmer': 1
        }
    },
    'Seal Chow': {
        'ingredients': {
            'Giant Clam': 1,
            'Driftwood': 1
        },
        'products': {
            'Seal Chow': 50
        }
    },
    'Trawl Net': {
        'ingredients': {
            'Giant Clam': 1,
            'Driftwood': 1,
            'Seaweed': 1
        },
        'products': {
            'Trawl Net': 25
        }
    },
    'Discount Card': {
        'ingredients': {
            'Driftwood': 1,
            '100% of Fisher\'s Discount': 1
        },
        'products': {
            'Discount Card': 1
        }
    },
    'Luck Stone': {
        'ingredients': {
            'Glass Bottle': 1,
            '100% of Fisher\'s Luck': 1
        },
        'products': {
            'Luck Stone': 1
        }
    }
}


class Recipe(NamedObject):

    def __init__(self, name, ingredients, products):
        self.ingredients = ingredients
        self.products = products
        NamedObject.__init__(self, name)

    def __str__(self):
        text = f'{self.name}\nIngredients: '
        for name, qty in self.ingredients.items():
            if qty > 1:
                text = f'{text}{name} x {qty}, '
            else:
                text = f'{text}{name}, '

        text = f'{text[:-2]}\nProducts: '
        for name, qty in self.products.items():
            if qty > 1:
                text = f'{text}{name} x {qty}, '
            else:
                text = f'{text}{name}, '

        return f'{text[:-2]}'

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        return Recipe(
            contents.get('name'),
            contents.get('ingredients'),
            contents.get('products')
        )

    def to_dict(self):
        """Return this object as a dict."""

        return {
            'name': self.name,
            'ingredients': self.ingredients,
            'products': self.products
        }
