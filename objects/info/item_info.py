from objects.info.item_dicts.basic_equipment import basic_equipment
from objects.info.item_dicts.breeding_items import breeding_items
from objects.info.item_dicts.crafting_ingredients import crafting_ingredients
from objects.info.item_dicts.defensive_items import defensive_items
from objects.info.item_dicts.frequency_items import frequency_items
from objects.info.item_dicts.knowledge_items import knowledge_items
from objects.info.item_dicts.legendary_relics import legendary_relics
from objects.info.item_dicts.simple_consumables import simple_consumables


class ItemInfo(dict):

    def update_all(self, dictionary_list):
        """Update this ItemInfo with all dictionaries in the list and return it."""

        for dictionary in dictionary_list:
            self.update(dictionary)
        return self


all_items = ItemInfo().update_all(
    [
        basic_equipment,
        simple_consumables,
        crafting_ingredients,
        legendary_relics,
        knowledge_items,
        breeding_items,
        defensive_items,
        frequency_items
    ]
)
