from functions.items.item_utils import add_item_by_name
from functions.logic.special_catches.relics import get_relic_data
from functions.text.logging import log_text


class FeatBook:
    feats = [
        '[00] Catch a SingleTuna.',
        '[01] Catch something over 3500 kg.',
        '[02] Catch something worth over $20000.00.',
        '[03] Catch over $100000.00 in one Trawl Net.',
        '[04] Catch 100 Treasure Chests.',
        '[05] Catch 50 Iridescent catchables.',
        '[06] Kill a God.',
        '[07] Cheat a Kraken.',
        '[08] Free a soul.',
        '[09] Save the turtles.',
        '[10] Return lost goods.',
        '[11] Collect all Legendary Relics.',
        '[12] Capture newborns of 10 different species.',
        '[13] Own $100000000.00.'
    ]

    def __init__(self):
        self.feat_list = [False] * len(FeatBook.feats)

    def toggle_feat(self, feat_num, state):
        """Toggle a feat on, giving output if it was previously off, and giving a book if needed."""

        if not self.feat_list[feat_num]:
            self.feat_list[feat_num] = True
            log_text(f'Achieved feat: {FeatBook.feats[feat_num]}\n')
            if not state.inventory.get('Feats of Fishing'):
                add_item_by_name(state, 'Feats of Fishing')
                log_text('A copy of "Feats of Fishing" materializes from thin air.'
                         '\nYou add it to your boat.\n')

    def count_achieved(self):
        """Return the number of achieved feats."""

        achieved = 0
        for feat in self.feat_list:
            if feat:
                achieved += 1
        return achieved

    @staticmethod
    def from_dict(contents):
        """Create this object from a dict."""

        feat_book = FeatBook()
        feat_book.feat_list = contents.get('feat_list')
        return feat_book

    def to_dict(self):
        """Return this object as a dict."""

        return {'feat_list': self.feat_list}


def achieve_feats(state, catch):
    """Achieve feats related to catching."""

    if catch.contains('SingleTuna'):
        state.feat_book.toggle_feat(0, state)

    if catch.weight > 3500:
        state.feat_book.toggle_feat(1, state)

    if catch.value > 20000:
        state.feat_book.toggle_feat(2, state)

    if state.chests >= 100:
        state.feat_book.toggle_feat(4, state)

    if state.iridescent >= 50:
        state.feat_book.toggle_feat(5, state)

    relic_list, owned, total = get_relic_data(state)
    if owned >= total:
        state.feat_book.toggle_feat(11, state)

    if state.money >= 100000000:
        state.feat_book.toggle_feat(13, state)
