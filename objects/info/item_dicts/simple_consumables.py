from objects.info.item_type import ItemType

simple_consumables = {
    'Scratch Ticket': {
        'use_type': ItemType.CONSUMABLE_CATCH_ALL, 'buy': 10, 'sell': -1,
        'text':
            'Provides a 1+ in 99999 chance to win one million dollars',
        'flavour':
            'Yo dawg, I heard you like random-number-generation based game mechanics...',
        'in_main_store': True
    },
    'Bait Shop Coupon': {
        'use_type': ItemType.CONSUMABLE_CATCH_ALL, 'buy': 2500, 'sell': -1,
        'text':
            'Increases your bait discount by $0.50',
        'flavour':
            'What kind of store sells persistent discounts for their own products?',
        'in_main_store': True
    },
    'Lucky Sea Hare\'s Foot': {
        'use_type': ItemType.CONSUMABLE_CATCH_ALL, 'buy': 5000, 'sell': -1,
        'text':
            'Makes you feel lucky',
        'flavour':
            'Yes, you have to eat it to obtain your luck.',
        'in_main_store': True
    },
    'Admiral Burger Combo': {
        'use_type': ItemType.CONSUMABLE_CATCH_ONE, 'buy': 50, 'sell': -1,
        'text':
            'Increases the base weight of your next catch',
        'flavour':
            'A 2 kg salmon patty fried in boat fuel and served with a small tricorn hat.',
        'in_main_store': True
    },
    'Colour Enhancing Flakes': {
        'use_type': ItemType.CONSUMABLE_CATCH_ONE, 'buy': 100, 'sell': -1,
        'text':
            'Increases the value of your next catch if it is Albino, Dark, or Gilded',
        'flavour':
            'Looks suspiciously like white glue with rainbow craft glitter.',
        'in_main_store': True
    }
}
