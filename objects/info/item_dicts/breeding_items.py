from objects.info.item_type import ItemType

breeding_items = {
    'Turtle Conservation Papers': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 25000, 'sell': -1,
        'text':
            'Licenses you to sell Sea Turtles freely',
        'flavour':
            'Save the planet, using the powers of fishing and money.',
        'in_main_store': True
    },
    'Portable Aquarium': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 4000, 'sell': -1,
        'text':
            'Allows you to keep Newborn fish as pets',
        'flavour':
            'Looks suspiciously like a mason jar.',
        'in_main_store': True
    },
    'Marine Creature Breeding Kit': {
        'use_type': ItemType.CONSUMABLE_CATCH_ONE, 'buy': 4000, 'sell': 400,
        'text': 'Breeds one pair of organisms stored in Portable Aquariums',
        'flavour':
            'It\'s a large tupperware with a tempting, aphrodisiac allure.  You resist, barely.',
        'in_main_store': False
    }
}
