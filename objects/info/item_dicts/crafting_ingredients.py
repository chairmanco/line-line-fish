from objects.info.item_type import ItemType

crafting_ingredients = {
    'Venom': {
        'use_type': ItemType.CRAFTING, 'buy': 5000, 'sell': -1,
        'text': 'Rare crafting ingredient',
        'flavour':
            'Venom, that\'s right, pedants.  I know the difference between venom and poison.',
        'in_main_store': False
    },
    'Driftwood': {
        'use_type': ItemType.CRAFTING, 'buy': 2000, 'sell': -1,
        'text': 'Common crafting ingredient',
        'flavour':
            'Is that driftwood in your boat inventory or are you just happy to see me?',
        'in_main_store': False
    },
    'Seaweed': {
        'use_type': ItemType.CRAFTING, 'buy': 2000, 'sell': -1,
        'text': 'Common crafting ingredient',
        'flavour':
            'Fun fact: In 2018, seaweed was legal in Canada.  It was also legal before then.',
        'in_main_store': False
    },
    'Scrap Metal': {
        'use_type': ItemType.CRAFTING, 'buy': 2000, 'sell': -1,
        'text': 'Common crafting ingredient',
        'flavour':
            'Something something tetanus, something something hepatitis B.',
        'in_main_store': False
    },
    'Glass Bottle': {
        'use_type': ItemType.CRAFTING, 'buy': 2000, 'sell': -1,
        'text': 'Common crafting ingredient',
        'flavour':
            'The only message in this bottle is a grim warning against disposable consumer goods.',
        'in_main_store': False
    },
    'Giant Clam': {
        'use_type': ItemType.CRAFTING, 'buy': 2000, 'sell': -1,
        'text': 'Common crafting ingredient',
        'flavour':
            'Why is this a crafting item and not a normal sea creature?  Blatant racism.',
        'in_main_store': False
    }
}
