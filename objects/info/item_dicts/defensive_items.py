from objects.info.item_type import ItemType

defensive_items = {
    'Artificial Mermaid': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 4000, 'sell': -1,
        'text':
            'Provides a blessing which defends against one shark attack',
        'flavour':
            '50% fish, 50% sea lady, 100% aspartame.',
        'in_main_store': True
    },
    'Fake Bait Shop Coupon': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 25, 'sell': -1,
        'text':
            'Does not increase your bait discount by $0.50',
        'flavour':
            'Coming soon: mock and stub bait shop coupons.',
        'in_main_store': True
    },
    'War Trident': {
        'use_type': ItemType.PERSISTENT_MULTI, 'buy': 16500, 'sell': -1,
        'text':
            'Adds a 25% chance to kill Bull Sharks and a 10% chance to kill Great'
            ' White Sharks',
        'flavour':
            'From hell\'s heart I stab at thee; for hate\'s sake I spit my last breath at thee.',
        'in_main_store': True
    },
    'Hungry Guard Seal': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 10000, 'sell': -1,
        'text':
            'Eats birds and mollusks, as well as 10% of your catch weight',
        'flavour':
            'Sea lions have ears.  Seals have no ears.  Hungry Guard Seals have no mercy.',
        'in_main_store': True
    },
    'Shark\'thulu\'s Bane': {
        'use_type': ItemType.PERSISTENT_MULTI, 'buy': 5500, 'sell': -1,
        'text':
            'Adds a 1% chance to defeat the Old God of the Sea',
        'flavour':
            '1/3 mermaids\' tears, 1/3 sharks\' blood, 1/3 lint from the Admiral\'s navel.',
        'in_main_store': True
    },
    'Lady of the Sea Statue': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 6000, 'sell': -1,
        'text':
            'Said to provide protection against omens, at a cost',
        'flavour':
            'She waited at the pier until she turned to stone.  Now you can buy her for six grand!',
        'in_main_store': True
    },
    'Anti-Venom': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 500, 'sell': -1,
        'text': 'Cures the effects of venomous stings and bites',
        'flavour':
            'Married to Uncle-Venom, daughter of Granny-Venom, third cousin once removed of Venny.',
        'in_main_store': False
    },
    'Krakengift': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 2000, 'sell': 200,
        'text': 'Poisons large mollusks with propensities for item theft',
        'flavour':
            'Tusoteuthis\' biology, Teutonic etymology.',
        'in_main_store': False
    }
}
