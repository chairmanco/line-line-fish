from objects.info.item_type import ItemType

legendary_relics = {
    'Legendary Infinite Anchovy': {
        'use_type': ItemType.PERSISTENT_RELIC, 'buy': 400000, 'sell': 4000,
        'text': 'All Anchovies are replaced with size 40-120 Anchovy Schools',
        'flavour':
            'Legends fear its name, unholy and unspoken: float(\'inf\') > float(\'inf\').',
        'in_main_store': False
    },
    'Legendary Bucket of Chum': {
        'use_type': ItemType.PERSISTENT_RELIC, 'buy': 400000, 'sell': 4000,
        'text':
            'Up to $1.00 per 10.00 kg of the previous catch can be used to purchase bait',
        'flavour':
            'Pirates love the three arrrghs: rum, riches, and recycling.',
        'in_main_store': False
    },
    'Legendary Soothing Seahorse': {
        'use_type': ItemType.PERSISTENT_RELIC, 'buy': 400000, 'sell': 4000,
        'text':
            'Being eaten by a shark no longer removes good luck or discounts',
        'flavour':
            'This creature produces lavender effervescence when placed in the bathtub.',
        'in_main_store': False
    },
    'Legendary Keys to the Yacht': {
        'use_type': ItemType.PERSISTENT_RELIC, 'buy': 400000, 'sell': 4000,
        'text':
            'Mermaids appear twice as frequently and multiple Mermaids can be stored',
        'flavour':
            'Upon use, your boat begins a magical transformation.  You also now wear boat shorts.',
        'in_main_store': False
    },
    'Legendary Permutation Barnacle': {
        'use_type': ItemType.PERSISTENT_RELIC, 'buy': 400000, 'sell': 4000,
        'text':
            'All catches have one additional modifier',
        'flavour':
            'Raised near Chernobyl and matured by Fukushima.  A healthy source of mutagens.',
        'in_main_store': False
    },
    'Legendary Violent Harpoon': {
        'use_type': ItemType.PERSISTENT_RELIC, 'buy': 400000, 'sell': 4000,
        'text':
            'You are immune the effects of frenzy and cannot be harmed by Bull Sharks',
        'flavour':
            'Harpoons: the great equalizer.  That and having a boat, tridents, and mermaid magic.',
        'in_main_store': False
    }
}
