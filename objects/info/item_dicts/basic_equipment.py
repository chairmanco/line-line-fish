from objects.info.item_type import ItemType

basic_equipment = {
    'Gutting Knife': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 200, 'sell': -1,
        'text':
            'Allows you to open Gut-Loaded catches',
        'flavour':
            'Hamlet: Act 2, Scene 2, Page 8',
        'in_main_store': True
    },
    'DIY Shipbuilding Kit': {
        'use_type': ItemType.CONSUMABLE_CATCH_ALL, 'buy': 6500, 'sell': -1,
        'text':
            'Increases inventory space by 10',
        'flavour':
            'Comes in a flat-pack with Allen keys, mini-dowels, and a Swedish language manual.',
        'in_main_store': True
    },
    'Trawl Net': {
        'use_type': ItemType.CONSUMABLE_CATCH_ONE, 'buy': 1500, 'sell': -1,
        'text':
            'Allows 10-30 catches with a single round of bait',
        'flavour':
            'The most efficient method to catch two and a half dozen Anchovies in a single stroke.',
        'in_main_store': True
    },
    'Venom Skimmer': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 8500, 'sell': -1,
        'text': 'Collects venom from venomous catches',
        'flavour':
            'Also reduces whole venom into 2%, 1%, and fat-free diet varieties.',
        'in_main_store': False
    },
    'Seal Chow': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 20, 'sell': -1,
        'text': 'Feeds hungry seals',
        'flavour':
            'Just add water.  Makes its own gravy!  Now with flavour!',
        'in_main_store': False
    },
    'Lobster Cage': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 12500, 'sell': -1,
        'text':
            'Allows you to catch Lobsters',
        'flavour':
            'The product formerly known as Lobsterlas Coppola.',
        'in_main_store': True
    }
}
