from objects.info.item_type import ItemType

knowledge_items = {
    'Feats of Fishing': {
        'use_type': ItemType.NO_TYPE, 'buy': 14, 'sell': 14,
        'text': 'A book which records achieved feats of fishing',
        'flavour':
            'For the finest fisher fears failing to find the fabled fourteen feats of fishing.',
        'in_main_store': True
    },
    'Poseidon\'s Laptop': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 20000, 'sell': -1,
        'text':
            'Displays detailed data on catch weight and value',
        'flavour':
            'It looks like a ThinkPad but with a barnacle instead of a pointing button mouse.',
        'in_main_store': True
    },
    'Fishing License': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 100, 'sell': 100,
        'text': 'Required for legal fish collection',
        'flavour':
            'Only an idiot would sell their fishing license in a fishing game.',
        'in_main_store': True
    },
    'Crafting for Dummies': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 1000, 'sell': -1,
        'text': 'A book which allows crafting and records known crafting recipes',
        'flavour':
            'Not a dummy?  Why don\'t you try crafting without this book?  Oh right, you can\'t.',
        'in_main_store': True
    },
    'Recipe Fortune Cookie': {
        'use_type': ItemType.CONSUMABLE_CATCH_ALL, 'buy': 500, 'sell': 50,
        'text': 'Contains a random unknown crafting recipe',
        'flavour':
            'This cookie\'s behaviour may or may not break the law of causality.',
        'in_main_store': True
    },
    'Love Letter to the Lady of the Sea': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 0.99, 'sell': 100,
        'text': 'A nice letter written for someone who has been waiting',
        'flavour':
            'Lobsters are red, marlins are blue, I said we would wed, but I died before you.',
        'in_main_store': False
    }
}
