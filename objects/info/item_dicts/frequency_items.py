from objects.info.item_type import ItemType

frequency_items = {
    'Whale Shark Repellent': {
        'use_type': ItemType.CONSUMABLE_CATCH_ONE, 'buy': 100, 'sell': -1,
        'text':
            'Decreases the chance of catching Whale Sharks',
        'flavour':
            'According to the ingredients, it\'s just diet pills mixed with krill.',
        'in_main_store': True
    },
    'Marlin Pheromones': {
        'use_type': ItemType.CONSUMABLE_CATCH_ONE, 'buy': 1000, 'sell': -1,
        'text':
            'Increases the chance of catching Marlins',
        'flavour':
            'Think the price is too high?  How do you think they extract this stuff?',
        'in_main_store': True
    },
    'Piscine Reducer': {
        'use_type': ItemType.PERSISTENT_MULTI, 'buy': 8500, 'sell': -1,
        'text':
            'Proportionally reduces the number of most sea creatures in the sea',
        'flavour':
            'There\'s other fish in the sea, unless you remove them.  Then there\'s just treasure.',
        'in_main_store': True
    },
    'Fisherman\'s Magnet': {
        'use_type': ItemType.PERSISTENT_MULTI, 'buy': 7500, 'sell': -1,
        'text':
            'After 20 rounds without catching a treasure, increase the chance of'
            ' catching treasure',
        'flavour':
            'Gold may not be magnetic, but chests have iron screws. - It\'s a VERY strong magnet.',
        'in_main_store': True
    },
    'Handsome Captain\'s Hat': {
        'use_type': ItemType.PERSISTENT_SINGLE, 'buy': 14500, 'sell': -1,
        'text':
            'Increases the chance of catching Mermaids',
        'flavour':
            'Imported from the finest haberdashery in Atlantis.',
        'in_main_store': True
    },
    'Puppy in a Raincoat': {
        'use_type': ItemType.CONSUMABLE_AUTO, 'buy': 100, 'sell': -1,
        'text':
            'Attracts Sharks and other large predators to your boat',
        'flavour':
            'He wears little Wellington boots to keep his paws dry.  At least until he dies.',
        'in_main_store': True
    }
}
