function getFisherCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function getUsername() {
    var username = getFisherCookie("fisher_username")
    if (username == "") {
        return "NONE"
    } else {
        return username
    }
}

function getPassword() {
    var password = getFisherCookie("fisher_password")
    if (password == "") {
        return "NONE"
    } else {
        return password
    }
}

function updateFisherName() {
    $('#Fisher').text(getUsername());
}

function getLogin() {
    return "login " + getUsername() + " " + getPassword()
}

function fishCommand(command_text, destination) {
    $(destination).html("Loading...");
    $.ajax({
        method: "GET",
            url: "../line_line_request_handler.py",
            data: {command_text: command_text},
            success: function(result) {
                $(destination).html(result);
            }
        });
    }
