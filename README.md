# Line Line Fish

Command Line based fishing.  Excellent time-waster.

"Cast your (fishing) line using the (command) line."


## Running Line Line Fish

    $ python line_line_fish.py new {user} {pass}
    $ python line_line_fish.py [login {user} {pass}] [args]

Arguments:

    # Key: required [optional] <number> {text}

    [fish] [<bait>]
    info
    reset
    [see] boat [<index>]
    [see] store [<index>]
    buy <index> [<qty>]
    sell <index> [<qty>]
    sink <index> [<qty>]
    craft <index> [<qty>]
    wish [<fish_index>] [<modifier_index>] [<modifier_index>] [<modifier_index>]
    view recipes
    view feats

E.g.

    $ python line_line_fish.py                  # Fish with no bait
    $ python line_line_fish.py 5                # Fish with 5 bait
    $ python line_line_fish.py info             # View current stats
    
    $ python line_line_fish.py store            # View the store
    $ python line_line_fish.py store 2          # View the second item in the store
    $ python line_line_fish.py buy 2 4          # Buy 4 units of the second item in the store
