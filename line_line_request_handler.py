#!/usr/bin/python
import cgi
import html

from functions.text.logging import get_log
from line_line_fish import process_login

form = cgi.FieldStorage()
command_text = form.getvalue('command_text')

command_vector = ['line_line_fish']
if command_text:
    received_vector = command_text.split(' ')
    command_vector.extend(received_vector)

process_login(command_vector)

print('Content-type:text/html\r\n\r\n')
for line in get_log():
    print(f'{html.escape(line)}<br>')
