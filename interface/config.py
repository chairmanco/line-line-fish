import json

from functions.text.logging import log_text
from interface.exit import exit_game

try:
    config = json.load(open('local_config.json', 'r'))
except (FileNotFoundError, IOError):
    try:
        config = json.load(open('config.json', 'r'))
    except (FileNotFoundError, IOError):
        log_text('Could not load config data.')
        exit_game()
