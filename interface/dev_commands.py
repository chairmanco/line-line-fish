from functions.actions.enqueue import enqueue_sunken_item, enqueue_fish
from functions.actions.grant import grant_money, grant_item, grant_tokens
from functions.actions.reveal import reveal_sea
from functions.actions.serialize import serialize_fisher, serialize_location
from functions.state.game_state import game_state
from functions.text.logging import log_text


def show_dev_commands():
    """Show dev commands."""

    if game_state.username != 'admin':
        return False

    log_text('\nDeveloper only arguments:')
    log_text('    grant money [<qty>]')
    log_text('    grant {item} [<qty>] [<sell_price>] [<buy_price>]')
    log_text('    grant tokens [<qty>]')
    log_text('    [en]queue {fish} [{modifier}] [{modifier}] [{modifier}]')  # Defaults to common
    log_text('    [en]queue sunken {item}')  # Defaults to common
    log_text('    reveal')
    log_text('    serialize fisher')
    log_text('    serialize location')


def dev_commands(args):
    """Process arguments as inputs in human-readable mode and execute developer commands."""

    if game_state.username != 'admin':
        return False

    try:
        qty = int(args[3])
    except (ValueError, IndexError):
        qty = 1

    try:
        sell_price = int(args[4])
    except (ValueError, IndexError):
        sell_price = 1

    try:
        buy_price = int(args[5])
    except (ValueError, IndexError):
        buy_price = 1

    if len(args) > 1 and args[1] == 'grant':
        if len(args) > 2:
            if args[2] == 'money':
                grant_money(qty)
                return True
            elif args[2] == 'tokens':
                grant_tokens(qty)
                return True
            else:
                grant_item(args[2], qty, sell_price, buy_price)
                return True

    try:
        fish = args[2]
    except IndexError:
        fish = ''

    try:
        first_mod = args[3]
    except IndexError:
        first_mod = 'Common'

    try:
        second_mod = args[4]
    except IndexError:
        second_mod = ''

    try:
        third_mod = args[5]
    except IndexError:
        third_mod = ''

    if len(args) > 1 and args[1] == 'enqueue' or args[1] == 'queue':
        if len(args) > 2:
            if args[2] == 'sunken':
                try:
                    enqueue_sunken_item(args[3])
                    return True
                except (ValueError, IndexError):
                    pass
            elif fish:
                enqueue_fish(fish, first_mod, second_mod, third_mod)
                return True

    if len(args) > 1 and args[1] == 'reveal':
        reveal_sea()
        return True

    if len(args) > 2 and args[1] == 'serialize' and args[2] == 'fisher':
        serialize_fisher()
        return True

    if len(args) > 2 and args[1] == 'serialize' and args[2] == 'location':
        serialize_location()
        return True

    return False


def fake_commands(args):
    """Process arguments as inputs in human-readable mode and responds to improper commands."""

    responses = {
        'ls': 'secret_files',
        'cd secret_files': 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        'cd': '',
        'rm secret_files': 'Don\'t touch that.',
        'rm -rf secret_files': 'Don\'t touch that.',
        'rm -rf .': 'Oh no!  You deleted Line Line Fish forever!',
        'rm -rf /': 'Oh no!  You deleted Line Line Fish forever!',
        'sudo': 'Nice try.'
    }

    for key, value in responses.items():
        tokens = key.split()
        index = 1
        for token in tokens:
            if len(args) <= index or token != args[index]:
                index = -1
                break
            index += 1
        if index > 0:
            log_text(value)
            return True

    return False
