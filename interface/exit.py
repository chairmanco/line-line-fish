import sys

from functions.text.logging import print_log


def exit_game():
    """Exit the game and output data in the correct manner."""

    print_log()
    sys.exit()
