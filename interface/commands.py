from functions.actions.view import view_recipes, view_feats
from interface.config import config
from interface.dev_commands import dev_commands, show_dev_commands, fake_commands
from functions.actions.craft import craft_recipe
from functions.actions.do_fish import do_fish
from functions.actions.wish import show_wish_data, do_wish
from functions.state.game_state import new_state, login
from functions.text.logging import log_text


def process_login(args):
    """Process login arguments if any then pass remaining inputs for processing."""

    if len(args) > 3 and args[1] == 'new':
        if new_state(args[2], args[3]):
            return log_text(f'Successfully created user {args[2]}.')
        else:
            return False

    if len(args) > 3 and args[1] == 'login':
        if args[2] == 'NONE':
            log_text('You need to log-in before using Line Line Fish!')
            return None
        if not login(args[2], args[3]):
            return False
        remaining_args = [args[0]]
        remaining_args.extend(args[4:])
        readable_args(remaining_args)
    else:
        readable_args(args)


def readable_args(args):
    """Process arguments as inputs in human-readable mode and execute player commands."""

    if len(args) == 1:
        return do_fish(0, False, False, False, False, False)

    if args[1] == 'info':
        if len(args) > 2:
            return do_fish(args[2], True, False, False, False, False)
        else:
            return do_fish(0, True, False, False, False, False)

    if args[1] == 'reset':
        return do_fish(0, False, True, False, False, False)

    if args[1] == 'see':
        return view_inventory(args[2:])

    if args[1] == 'boat' or args[1] == 'store':
        return view_inventory(args[1:])

    try:
        qty = int(args[3])
    except (ValueError, IndexError):
        qty = 1

    try:
        index = int(args[2])
    except (ValueError, IndexError):
        index = -1

    if args[1] == 'buy' and len(args) > 2:
        return do_fish(index, False, False, False, True, qty)

    if args[1] == 'sell' and len(args) > 2:
        return do_fish(index, False, False, True, False, qty)

    if args[1] == 'sink' and len(args) > 2:
        return do_fish(index, False, False, True, False, -qty)

    if args[1] == 'craft' and len(args) > 2:
        return craft_recipe(index, qty)

    if args[1] == 'wish' and len(args) == 2:
        return show_wish_data()

    if args[1] == 'wish' and len(args) > 2:
        try:
            fish = int(args[2])
        except (ValueError, IndexError):
            fish = -1

        try:
            first_mod = int(args[3])
        except (ValueError, IndexError):
            first_mod = -1

        try:
            second_mod = int(args[4])
        except (ValueError, IndexError):
            second_mod = -1

        try:
            third_mod = int(args[5])
        except (ValueError, IndexError):
            third_mod = -1

        return do_wish(fish, first_mod, second_mod, third_mod)

    if args[1] == 'view' and len(args) > 2:
        if args[2] == 'recipes':
            return view_recipes()

        if args[2] == 'feats':
            return view_feats()

    if config.get('ALLOW_DEV_COMMANDS') and dev_commands(args):
        return True

    if fake_commands(args):
        return True

    if args[1] == 'fish' and len(args) == 2:
        return do_fish(0, False, False, False, False, False)

    if args[1] == 'fish':
        try:
            bait = int(args[2])
            return do_fish(bait, False, False, False, False, False)
        except (ValueError, IndexError):
            pass

    try:
        bait = int(args[1])
        return do_fish(bait, False, False, False, False, False)
    except (ValueError, IndexError):
        show_valid_commands()


def view_inventory(args):
    """View item(s) from the boat or store."""

    if len(args) == 0:
        return show_valid_commands()
    else:
        index = -1
        if len(args) > 1:
            try:
                index = int(args[1])
            except ValueError:
                pass
        if args[0] == 'boat':
            return do_fish(index, False, False, True, False, False)
        if args[0] == 'store':
            return do_fish(index, False, False, False, True, False)


def show_valid_commands():
    """Show valid commands."""
    log_text('Key: required [optional] <number> {text}\n')

    log_text('Command Syntax:')
    log_text('    new {user} {pass}')
    log_text('    [login {user} {pass}] [args]\n')

    log_text('Valid arguments:')
    log_text('    [fish] [<bait>]')
    log_text('    info')
    log_text('    reset')
    log_text('    [see] boat [<index>]')
    log_text('    [see] store [<index>]')
    log_text('    buy <index> [<qty>]')
    log_text('    sell <index> [<qty>]')
    log_text('    sink <index> [<qty>]')
    log_text('    craft <index> [<qty>]')
    log_text('    wish [<fish_index>] [<modifier_index>] [<modifier_index>] [<modifier_index>]')
    log_text('    view recipes')
    log_text('    view feats')
    if config.get('ALLOW_DEV_COMMANDS'):
        show_dev_commands()
