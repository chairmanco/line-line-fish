import math
import random

from functions.generation.reduction import reduce
from functions.items.item_utils import is_overloaded, decrement_item_by_name
from functions.text.formatting import percentage
from functions.text.logging import log_text
from objects.data.fish_data.fishes import Fishes
from objects.data.freq_mods.freq_mods import FreqMods
from objects.data.freq_mods.multiply_freq import MultiplyFreq
from objects.data.location.location import Location
from objects.data.seas.static_sea import StaticSea


def generate_sea(state, bait):
    """Use items."""

    sea = StaticSea.definitive()

    sea.apply_freq_mods(
        FreqMods(
            [
                ('add', 'Treasure Chest',
                 1 * (state.luck * (state.luck > 0)) + state.treasure_boost),
                ('add', 'Legendary Relic',
                 1 * (state.luck * (state.luck > 0)) + state.treasure_boost)
            ]
        )
    )

    reduction = get_weather_reduction(state)
    add_reducible_catches(state, sea, bait, reduction)
    apply_item_modifiers(state, sea)
    apply_location_modifiers(state, sea)
    add_krakens(state, sea)

    return sea


def apply_location_modifiers(state, sea):
    """Apply modifiers from locations."""

    location = state.location_object

    if isinstance(location, Location):
        sea.apply_freq_mods(location.static_sea_modifiers)
        sea.apply_freq_mods(location.weather.freq_mods)


def get_weather_reduction(state):
    """Get the weather's reduction power."""

    location = state.location_object

    if isinstance(location, Location):
        return location.weather.reduction_power


def add_krakens(state, sea):
    """Add Krakens from boat overloading."""

    excess_items = is_overloaded(state.inventory, state.ship_size)
    if excess_items:
        log_text(f'Your ship is {percentage(excess_items + state.ship_size, state.ship_size)}'
                 f' overloaded and is struggling to move!')
        log_text('The sea may send horrors from the deep to punish you for your hubris.\n')
        sea.add_simple(
            'Kraken of Grand Larceny',
            math.floor(sea.get_total() * excess_items)
        )


def apply_item_modifiers(state, sea):
    """Apply modifiers from items."""

    if state.inventory.get('Legendary Keys to the Yacht'):
        sea.apply_freq_mod(MultiplyFreq('Mermaid', 2))

    item = state.inventory.get('Whale Shark Repellent')
    if item:
        log_text(f'You sprayed Whale Shark Repellent into the sea. ({item.qty - 1} remaining)\n')
        decrement_item_by_name(state, item.name)
        sea.apply_freq_mod(MultiplyFreq('Whale Shark', 1 / 2))

    item = state.inventory.get('Marlin Pheromones')
    if item:
        log_text(f'You sprayed Marlin Pheromones into the sea. ({item.qty - 1} remaining)\n')
        decrement_item_by_name(state, item.name)
        sea.apply_freq_mod(MultiplyFreq('Marlin', 2))

    if state.inventory.get('Handsome Captain\'s Hat'):
        sea.apply_freq_mod(MultiplyFreq('Mermaid', 2))

    if state.inventory.get('Puppy in a Raincoat'):
        sea.apply_freq_mod(MultiplyFreq('Bull Shark', 2))
        sea.apply_freq_mod(MultiplyFreq('Great White Shark', 2))
        sea.apply_freq_mod(MultiplyFreq('Shark\'thulu', 10))
        sea.apply_freq_mod(MultiplyFreq('Kraken of Grand Larceny', 10))


def add_reducible_catches(state, sea, bait, base_reduction):
    """Add catches which interact with reducers."""

    reduction = base_reduction
    item = state.inventory.get('Piscine Reducer')
    if item:
        reduction = item.qty

    for name, fish in Fishes().definitive().items():
        sea.add_simple(
            name,
            sea.get_with_default_key(name, 'Default').freq or
            reduce(
                math.floor(fish.weight ** (0 + bait / 50) * fish.freq),
                reduction,
                fish.freq / 100
            )
        )

    if state.inventory.get('Turtle Conservation Papers'):  # TODO: Rework buying papers vs breeding
        sea.remove_frequency('Endangered Sea Turtle')
        sea.add_simple(
            'No-Longer-Endangered Sea Turtle',
            reduce(
                math.floor(Fishes.definitive()
                           .get('No-Longer-Endangered Sea Turtle').weight
                           ** (0 + bait / 50) * 5),
                reduction,
                1
            )
        )

    if state.inventory.get('Lobster Cage'):
        log_text('Your Lobster Cage is in prime operating condition.\n')
        sea.add_simple(
            'Spiny Lobster',
            reduce(
                math.floor(Fishes.definitive().get('Spiny Lobster').weight
                           ** (0 + bait / 50) * 400),
                reduction,
                4
            )
        )
        sea.add_simple(
            'American Lobster',
            reduce(
                math.floor(Fishes.definitive().get('American Lobster').weight
                           ** (0 + bait / 50) * 200),
                reduction,
                2
            )
        )
    elif state.inventory.get('Buckled Lobster Cage'):
        log_text('Your Lobster Cage is in no condition to catch any Lobsters.\n')


def choose_sea(first, second):
    """Perform a weighted choice between two seas and return it and 0 or 1 accordingly."""

    first_len = len(first)
    second_len = len(second)

    choice = random.randrange(0, first_len + second_len)

    if choice < first_len:
        return first, 0
    else:
        return second, 1
