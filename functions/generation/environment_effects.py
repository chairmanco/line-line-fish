from functions.text.logging import log_text
from objects.data.fisher import Fisher
from objects.data.location.location import Location


def process_environment(state):
    """Handle environmental effects and info."""

    if isinstance(state, Fisher):
        location = state.location_object
        if isinstance(location, Location):
            location.advance_weather()  # TODO: This may need to be a timestamp thing
            log_text(f'Location: {location}')
            log_text(f'Weather Conditions: {location.weather} ({location.weather_remaining})'
                     f', Wind: {location.wind}\n')
