import math
import random

from functions.generation.reduction import is_reducible
from functions.logic.special_catches.sharks import defeat_shark, defeat_thulu
from functions.text.conditional_text import cond_string
from functions.text.formatting import dollars, kilograms
from functions.text.logging import log_text
from objects.data.fish_data.catch_data import CatchData
from objects.data.fish_data.fishes import Fishes
from objects.data.fish_data.modifiers import Modifiers
from objects.data.sea_objects.full_catch import FullCatch
from objects.data.sea_objects.modified_catch import ModifiedCatch
from objects.data.sea_objects.simple_catch import SimpleCatch
from objects.data.catch_queue import CatchQueue


def generate_catch_data(state, sea):
    """Generate a catch and build it if required."""

    sea_object = get_sea_object(state, sea)
    modified_catch = None

    if isinstance(sea_object, CatchData):
        return sea_object

    if isinstance(sea_object, SimpleCatch):
        modified_catch = build_modified_from_simple(sea_object)

    if isinstance(sea_object, ModifiedCatch):
        modified_catch = sea_object

    if modified_catch:
        modified_catch = rebuild_modified_with_specials(state, modified_catch)
        return build_full_from_modified(state, modified_catch)

    return None


def get_sea_object(state, sea):
    """Get a SeaObject from the queue if it has data, or from a sea otherwise."""

    if isinstance(state.catch_queue, CatchQueue) and not state.catch_queue.empty():
        return state.catch_queue.get()
    else:
        return sea.fish_from_sea()


def build_modified_from_simple(simple_catch):
    """Create a modified catch from a simple catch by giving it a random modifier."""

    modifier_name = Modifiers.definitive().get_random_key()

    catch = ModifiedCatch(
        '',
        1,
        simple_catch.fish_name,
        [modifier_name]
    )

    return catch


def rebuild_modified_with_specials(state, modified_catch):
    """Apply special effects to a modified catch."""

    catch = ModifiedCatch(
        '',
        1,
        modified_catch.fish_name,
        modified_catch.modifier_names
    )

    if state.inventory.get('Legendary Permutation Barnacle'):
        extra_mod = Modifiers.definitive().get_random_value().name
        catch.add_modifier(extra_mod)

    return catch


def build_full_from_modified(state, modified_catch, analyze=False, sea=None, bait=0):
    """Create a full catch from a modified catch by generating weight and value."""

    if not isinstance(modified_catch, ModifiedCatch):
        return None

    fish = Fishes.definitive().get_with_default_key(modified_catch.fish_name, 'Default')
    modifier = modified_catch.extract_modifier()

    varies = int('Chest' not in fish.name)
    weight = fish.weight
    weight_history = [kilograms(weight)]
    luck = state.luck

    if state.burger:
        weight = weight * 1.1
        weight_history.append(f'{kilograms(weight)} (Burger)')

    for i in range(random.randrange(5, 11)):
        variances = [weight]
        variances += [weight - (weight / 10.0)] * (1 - luck * (luck < 0)) * varies
        variances += [weight + (weight / 10.0)] * (1 + luck * (luck > 0)) * varies
        weight = random.choice(variances)
        weight_history.append(kilograms(weight))

    value_boost = 0
    if state.flakes:
        for modifier_name in modified_catch.modifier_names:
            if modifier_name in ('Albino', 'Dark', 'Gilded'):
                value_boost = 0.3
                break

    weight = round(weight * modifier.weight, 2)
    value = round(weight * fish.value * (modifier.value + value_boost), 2)
    name = f'{modifier.name} {fish.name}'

    kill_value = get_kill_value(state, fish)
    if kill_value:
        value = kill_value
        value_text = f'{dollars(fish.value)} (Killed)'
    else:
        value_text = f'{dollars(fish.value)}'

    if analyze and sea:
        log_text('\nAnalyzing catch...')
        log_text(f'Species: {fish.name},'
                 f' {kilograms(fish.weight)} at {value_text} per kg')
        log_text(f'Modifier: {modifier.name}, x{modifier.weight} kg, x'
                 f'{modifier.value}{cond_string(f"+{value_boost}", value_boost)} $')
        if sea:
            log_text(f'Occurrence Rate: {fish.freq} base,'
                     f' {math.floor(fish.weight ** (0 + bait / 50) * fish.freq)} at {bait} bait,'
                     f' {sea.get(fish.name).freq} actual')
        log_text(f'Affected by Reducers: {is_reducible(state, fish.name)}')
        log_text(f'Weight history:\n{weight_history}\n')

    return FullCatch(
        name,
        weight,
        value,
        1,
        fish,
        [modifier]
    )


def get_kill_value(state, fish):
    """Get the value of a sea creature if it is killed."""

    if defeat_shark(state, 'Bull Shark') and fish.contains('Bull Shark'):
        return 2

    if defeat_shark(state, 'Great White Shark') and fish.contains('Great White Shark'):
        return 2

    if defeat_thulu(state) and fish.contains('Shark\'thulu'):
        return 5
