import math

from objects.data.fish_data.fishes import Fishes


def reduce(initial, reduction_count, lower_bound, base=2):
    """Reduce a value exponentially."""

    if reduction_count <= 0:
        return initial

    reduced = initial
    percent_minus = 0
    for c in range(reduction_count):
        if reduced > 0:
            percent_minus += math.log(reduced)
            reduced = reduced * (100 - math.log(reduced) / math.log(base)) / 100

    return max(math.floor(initial * (100 - percent_minus) / 100), max(math.floor(lower_bound), 1))


def is_reducible(state, fish_name):
    """Return whether a catchable can be reduced."""

    if Fishes.definitive().get(fish_name).freq > 0:
        return True

    if state.inventory.get('Lobster Cage') and 'Lobster' in fish_name:
        return True

    if state.inventory.get('Turtle Conservation Papers') and 'Turtle' in fish_name:
        return True

    return False
