import random

from interface.config import config
from functions.generation.environment_effects import process_environment
from functions.generation.generate_sea import generate_sea, choose_sea
from functions.generation.get_catch import generate_catch_data
from functions.items.item_utils import add_item_from_catch, decrement_item_by_name
from functions.items.use_items import use_consumable_all, use_consumable_single
from functions.logic.bait import compute_bait, compute_chum
from functions.logic.breeding import use_breeding_kits
from functions.logic.fish_stats import process_weight_value
from functions.logic.modifier_effects.frenzy import process_frenzy
from functions.logic.modifier_effects.gut_loaded import process_gut_loaded
from functions.logic.modifier_effects.newborn import process_newborn
from functions.logic.modifier_effects.wise import process_wise
from functions.logic.seals import process_seal
from functions.logic.sinking import test_for_sinking, warn_of_sinking
from functions.logic.special_catches.mermaids import apply_mermaid, process_mermaid
from functions.logic.special_catches.mollusks import process_mollusk
from functions.logic.special_catches.relics import process_relics
from functions.logic.special_catches.sharks import set_anti_shark_power, process_shark
from functions.logic.special_catches.star import process_star
from functions.logic.special_catches.treasure import process_treasure
from functions.logic.special_catches.turtles import process_turtle
from functions.logic.special_catches.venom import process_venom
from functions.logic.statue import process_statue
from functions.text.conditional_text import cond_print
from functions.text.formatting import dollars, kilograms
from functions.text.logging import log_text
from objects.data.sea_objects.item_catch import ItemCatch
from objects.info.feat_book import achieve_feats


def fish_catches(bait, state):
    """Catch and process random catchables."""

    initial_funds = state.money
    log_text()

    apply_mermaid(state)
    use_consumable_all(state)
    set_anti_shark_power(state)
    process_environment(state)

    if test_for_sinking(state):
        return True

    cost = (10 - state.discount) * (state.discount < 10)

    bait_used = compute_bait(bait, state.money, cost)
    spent = -bait_used * cost
    chum_discount = compute_chum(state, -spent)
    spent += chum_discount
    state.money += spent

    catches = 1

    use_consumable_single(state)
    use_breeding_kits(state)

    item = state.inventory.get('Trawl Net')
    if item:
        log_text(f'You used a trawl net. ({item.qty - 1} remaining)')
        decrement_item_by_name(state, item.name)
        catches = random.randrange(10, 31)
        log_text()

    summary = []
    for catch in range(catches):
        summary.append(fish_single_catch(bait_used, state, spent, chum_discount))

    net_gain = state.money - initial_funds

    if catches > 1:
        log_text(f'\n---\n\nSummary ({catches} catches):')
        for catch in summary:
            log_text(catch)
        log_text(f'\nTotal Funds: {dollars(state.money)}'
                 f' - Net Gain: {dollars(net_gain)}')

    if catches > 1 and net_gain > state.biggest_net[0]:
        state.biggest_net = (net_gain, catches)

    if net_gain >= 100000:
        state.feat_book.toggle_feat(3, state)

    warn_of_sinking(state)


def fish_single_catch(bait_used, state, spent, chum_discount):
    """Catch and process a random catchable."""

    apply_mermaid(state)
    nearby_sea = generate_sea(state, bait_used)
    persistent = state.persistent_sea

    if config.get('FORCE_PERSISTENT_CATCH'):
        sea = persistent
    else:
        sea, is_persistent = choose_sea(nearby_sea, persistent)

    show_bait_info(bait_used, spent, chum_discount, nearby_sea.get_total() + persistent.get_total())

    catch = generate_catch_data(state, sea)

    create_school(catch, state)

    summary = make_summary(catch)

    log_text(f'{summary}\n')

    add_item_from_catch(state, catch)
    seal_message = process_seal(state, catch)
    summary = make_summary(catch)

    process_newborn(state, catch)
    warded = process_statue(state, catch, sea)

    state.money += catch.value
    state.caught += 1
    state.weight += catch.weight
    state.chum = catch.weight
    state.luck += (catch.count('Luck')) ** 3
    state.luck -= (((catch.count('Omen') * 2) ** 2)
                   * (not (catch.contains('Albatross')
                           and state.inventory.get('Hungry Guard Seal')))
                   * (not warded)
                   )
    state.discount += (catch.count('Discount')) ** 3
    state.iridescent += catch.count('Iridescent')
    state.whale_shark += catch.weight * catch.count('Whale Shark')

    cond_print('The footwear is old and worn, but its sacred leather still carries its worth.\n',
               catch.contains('Boot'))
    cond_print('Only a foolish fisherman would cast their line so close to the heavens.\n',
               catch.contains('Albatross') and not state.inventory.get('Hungry Guard Seal'))
    cond_print('The bait shop owner will be glad to have his trusty weapon back.\n',
               catch.contains('Matchlock'))

    process_mollusk(state, catch)
    process_treasure(state, catch)
    process_relics(state, catch)
    process_turtle(state, catch)
    process_gut_loaded(state, catch)
    process_mermaid(state, catch)
    process_frenzy(state, catch)
    process_venom(state, catch, warded)
    process_star(state, catch)
    process_wise(state, catch)

    shark_slain = process_shark(state, catch)
    cond_print(seal_message, seal_message and not (catch.contains('Shark') and not shark_slain))

    cond_print('Your luck has suffered.\n', catch.contains('Omen') and not warded
               and not (catch.contains('Albatross') and state.inventory.get('Hungry Guard Seal')))
    cond_print('Your luck has improved.\n',
               catch.contains('Luck'))
    cond_print('Your bait discount has increased.\n', catch.contains('Discount'))
    process_weight_value(state, catch)

    achieve_feats(state, catch)

    log_text(f'Total Funds: {dollars(state.money)}')

    state.history[catch.name] = (state.history.get(catch.name) or 0) + 1
    return summary


def show_bait_info(bait_used, spent, chum_discount, nearby_fish):
    """Show data related to bait."""

    nearby_info = f'There are {nearby_fish} fish nearby.\n'

    if chum_discount:
        log_text(f'Saved {dollars(chum_discount)} using chum.')

    if bait_used:
        log_text(f'Bait: {bait_used} ({dollars(spent, True)}); {nearby_info}')
    else:
        log_text(f'Bait: {bait_used}; {nearby_info}')


def make_summary(catch):
    """Create summary text for a catch."""

    if isinstance(catch, ItemCatch):
        return (f'You caught: {catch.name}, {kilograms(catch.weight)},'
                f' sell price {dollars(catch.get_item().get_sell_price())}.')
    else:
        return (f'You caught: {catch}, {kilograms(catch.weight)},'
                f' worth {dollars(catch.value)}.')


def create_school(catch, state):
    """Convert a catch to a school if applicable."""

    if catch.contains('Anchovy') and state.inventory.get('Legendary Infinite Anchovy'):
        size = random.randrange(40, 121)
        catch.value *= size
        catch.weight *= size
        catch.name = f'School of {size} {catch.name.replace("Anchovy", "Anchovies")}'
