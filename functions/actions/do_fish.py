from functions.actions.show_info import show_info
from functions.generation.fish_catches import fish_catches
from functions.items.item_utils import add_item_by_name
from functions.items.management import manage_items
from functions.state.game_state import load_state, save_state
from functions.text.conditional_text import cond_string, cond_print
from functions.text.formatting import dollars, a_an
from functions.text.logging import log_text


def do_fish(bait, info, reset, boat, store, execute):
    """Analyze or obtain piscine information."""

    state = load_state(reset)

    log_text(f'"{state.get_descriptor()}"')

    cast_successful = False
    if state.inventory.get('Fishing License') and not any((info, reset, boat, store, execute)):
        cast_successful = state.time_tokens.get_token()

    casts = state.time_tokens.add_tokens()
    if casts == 1:
        log_text(f'Fishing Line: {casts} cast remaining')
    else:
        log_text(f'Fishing Line: {casts} casts remaining')

    log_text(f'Funds: {dollars(state.money)}'
             + cond_string(f'; Discount: {dollars(state.discount)}',
                           state.discount))

    cond_print(f'You feel lucky{"!" * state.luck}', state.luck > 0)
    cond_print(f'You don\'t feel very lucky{"." * abs(state.luck)}', state.luck < 0)
    cond_print(f'{a_an(state.mermaid, True)} {state.mermaid} is protecting you.',
               state.mermaid)
    cond_print('User data successfully reset.', reset)

    if info:
        show_info(state)

    if any((store, boat, execute)):
        manage_items(boat, store, state, bait, execute)

    if not any((info, reset, boat, store, execute)):
        if state.inventory.get('Fishing License'):
            if cast_successful:
                fish_catches(bait, state)
            else:
                log_text('\nYou need to wait and replace your line before fishing again!')
        else:
            log_text('\nYou need a Fishing License to do that!')
            if state.favour:
                log_text('\nA voice booms from the briny deep:'
                         '\n"You returned to me what I had lost.'
                         '\nI shall do the same for you."')
                state.favour = False
                add_item_by_name(state, 'Fishing License')

    save_state(state)
