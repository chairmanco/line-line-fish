import json

from functions.state.game_state import load_state
from functions.text.logging import log_text


def serialize_fisher():
    """Serialize and print the fisher."""

    state = load_state()
    log_text(json.dumps(state.to_dict(), indent=4, sort_keys=False))


def serialize_location():
    """Serialize and print the fisher's location."""

    state = load_state()
    log_text(json.dumps(state.location_object.to_dict(), indent=4, sort_keys=False))
