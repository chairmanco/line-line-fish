from functions.state.game_state import load_state, save_state
from functions.text.logging import log_text


def reveal_sea():
    """Reveals the persistent sea."""

    state = load_state()

    for name, element in state.persistent_sea.items():
        log_text(f'{name}: {element.freq}')

    save_state(state)
