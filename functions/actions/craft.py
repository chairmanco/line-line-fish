from functions.actions.special_crafting import craft_discount_card, craft_luck_stone
from functions.items.item_utils import add_item_by_name, get_qty_of_name, decrement_item_by_name
from functions.state.game_state import load_state, save_state
from functions.text.logging import log_text


def craft_recipe(index, qty=1):
    """Craft a recipe."""

    state = load_state()

    batches = 0
    while batches < qty and craft_one_batch(state, index):
        batches += 1

    save_state(state)


def craft_one_batch(state, index):
    """Craft one batch of a recipe."""

    if index < len(state.recipe_book) and state.recipe_book.recipe_list[index]:
        recipe = state.recipe_book.get_all_recipes()[index]

        if recipe.name == 'Discount Card':
            return craft_discount_card(state, index, recipe)

        if recipe.name == 'Luck Stone':
            return craft_luck_stone(state, index, recipe)

        success = True
        for name, qty in recipe.ingredients.items():
            if get_qty_of_name(state, name) < qty:
                success = False

        if success:
            for name, qty in recipe.ingredients.items():
                decrement_item_by_name(state, name, qty)
                log_text(f'Consumed {name} x {qty}.')

            for name, qty in recipe.products.items():
                add_item_by_name(state, name, qty)
                log_text(f'Crafted {name} x {qty}.')
            log_text()
            return True
        else:
            log_text(f'Insufficient resources to craft recipe: [{index}] {recipe.name}')
    else:
        log_text('That recipe is unknown or does not exist!')

    log_text()
    return False
