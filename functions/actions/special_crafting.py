import math

from functions.items.item_utils import get_qty_of_name, decrement_item_by_name
from functions.text.logging import log_text
from objects.info.item_type import ItemType


def craft_discount_card(state, index, recipe):
    """Craft a discount card."""

    success = state.discount != 0
    for name, qty in recipe.ingredients.items():
        if name != '100% of Fisher\'s Discount' and get_qty_of_name(state, name) < qty:
            success = False

    if success:
        level = state.discount * 2
        card_value = 10 ** (math.log(state.discount) / math.log(2.5))

        for name, qty in recipe.ingredients.items():
            if name == '100% of Fisher\'s Discount':
                log_text(f'Consumed {state.discount} bait discount points.')
                state.discount = 0
            else:
                decrement_item_by_name(state, name, qty)
                log_text(f'Consumed {name} x {qty}.')

        name = f'Level {int(level)} Discount Card'
        state.inventory.add_item(
            name,
            1,
            {
                'use_type': ItemType.NO_TYPE,
                'buy': 0,
                'sell': card_value,
                'text': f'A discount card which can be sold to other fishermen'
                '\n\n"Value == 10^(log(X)/log(2.5)).  You\'re welcome, no need to do math now."',
            }
        )

        log_text(f'Crafted {name} x 1.\n')
        return True
    else:
        log_text(f'Insufficient resources to craft recipe: [{index}] {recipe.name}\n')
        return False


def craft_luck_stone(state, index, recipe):
    """Craft a luck stone which is immediately dropped into the sea."""

    success = state.luck != 0
    for name, qty in recipe.ingredients.items():
        if name != '100% of Fisher\'s Luck' and get_qty_of_name(state, name) < qty:
            success = False

    if success:
        level = abs(state.luck)
        good = state.luck > 0

        for name, qty in recipe.ingredients.items():
            if name == '100% of Fisher\'s Luck':
                log_text(f'Consumed {state.luck} units of luck.')
                state.luck = 0
            else:
                decrement_item_by_name(state, name, qty)
                log_text(f'Consumed {name} x {qty}.')

        name = f'Level {int(level)} {"Good" if good else "Bad"} Luck Stone'
        log_text(f'Crafted {name} x 1.\n')

        log_text(f'The {name} glows, then lifts up from your hands.')
        log_text(f'It hovers over the sea before plunging straight down.')

        if good:
            log_text(f'The starlight seems to be coming from below this night.\n')
            state.persistent_sea.increase_frequency(
                'Brittle Star', level * 100
            )
        else:
            log_text(f'The crashing of the waves almost sounds like hissing.\n')
            state.persistent_sea.increase_frequency(
                'Sea Snake', level * 100
            )

        return True
    else:
        log_text(f'Insufficient resources to craft recipe: [{index}] {recipe.name}\n')
        return False
