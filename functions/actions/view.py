from functions.state.game_state import load_state, save_state
from functions.text.logging import log_text


def view_recipes():
    """View recipes."""

    state = load_state()

    log_text(f'Recipes Known:')
    for i in range(len(state.recipe_book.get_all_recipes())):
        if state.recipe_book.recipe_list[i]:
            log_text(f'[{i}] {state.recipe_book.get_all_recipes()[i]}\n')

    save_state(state)


def view_feats():
    """View feats."""

    state = load_state()

    log_text(f'Feats:')
    for i in range(len(state.feat_book.feats)):
        if state.feat_book.feat_list[i]:
            log_text(f'{state.feat_book.feats[i]} [DONE]')
        else:
            log_text(f'{state.feat_book.feats[i]}')

    save_state(state)
