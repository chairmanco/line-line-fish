from functions.items.item_utils import get_item_of_name
from functions.state.game_state import load_state, save_state
from functions.text.logging import log_text
from objects.data.fish_data.fish import Fish
from objects.data.fish_data.fishes import Fishes
from objects.data.fish_data.modifier import Modifier
from objects.data.fish_data.modifiers import Modifiers
from objects.data.items.inventory_item import InventoryItem
from objects.data.sea_objects.modified_catch import ModifiedCatch
from objects.data.sea_objects.sunken_item import SunkenItem
from objects.data.catch_queue import CatchQueue
from objects.info.item_type import ItemType


def enqueue_fish(fish, first_mod='Common', second_mod='', third_mod=''):
    """Add a fish-modifier pair to the catch queue."""

    fish = fish.replace('_', ' ')
    first_mod = first_mod.replace('_', ' ')
    second_mod = second_mod.replace('_', ' ')
    third_mod = third_mod.replace('_', ' ')

    state = load_state()

    next_data = set_next_fish_data(state, fish, first_mod, second_mod, third_mod)

    log_text(f'Enqueued {next_data.generate_name()}.')

    save_state(state)


def set_next_fish_data(state, fish, first_mod='Common', second_mod='', third_mod=''):
    """Add a fish-modifier pair to the catch queue in a state."""

    next_fish = Fishes.definitive().get(fish, None)
    if not next_fish:
        next_fish = Fish(
            fish, 1, 1, 1
        )

    next_modifiers = []
    if first_mod:
        next_modifiers.append(first_mod)
    if second_mod:
        next_modifiers.append(second_mod)
    if third_mod:
        next_modifiers.append(third_mod)

    next_data = ModifiedCatch(
        '',
        1,
        next_fish.name,
        next_modifiers
    )

    if isinstance(state.catch_queue, CatchQueue):
        state.catch_queue.put(next_data)
    else:
        state.catch_queue = CatchQueue()
        state.catch_queue.put(next_data)

    return next_data


def get_modifier(modifier):
    """Get a modifier if it exists, or generate a default one."""

    next_modifier = Modifiers.definitive().get(modifier, None)
    if not next_modifier:
        next_modifier = Modifier(
            modifier, 1, 1, 1
        )
    return next_modifier


def enqueue_sunken_item(item_name):
    """Add a sunken item to the catch queue."""

    item_name = item_name.replace('_', ' ')
    state = load_state()
    set_next_sunken_item(state, item_name)
    log_text(f'Enqueued Sunken Item: {item_name}.')
    save_state(state)


def set_next_sunken_item(state, item_name):
    """Add a sunken item to the catch queue in a state."""

    item = get_item_of_name(item_name)
    if not item:
        item = InventoryItem(
            item_name,
            1,
            {
                'use_type': ItemType.NO_TYPE,
                'buy': 1,
                'sell': 1,
                'text': f'Custom item with no description\n\n\"Custom item flavour text.\"'
            }
        )

    next_data = SunkenItem(
        item.name,
        1,
        item
    )

    if isinstance(state.catch_queue, CatchQueue):
        state.catch_queue.put(next_data)
    else:
        state.catch_queue = CatchQueue()
        state.catch_queue.put(next_data)
