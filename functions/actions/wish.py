import random

from functions.actions.enqueue import set_next_fish_data
from functions.state.game_state import load_state, save_state
from functions.text.formatting import percentage
from functions.text.logging import log_text
from objects.data.fish_data.fishes import Fishes
from objects.data.fish_data.modifiers import Modifiers


def get_valid_fishes():
    """Return a list of valid fish wishes."""

    valid = []
    banned = ['Legendary Relic', 'Sunken Item', 'Default']

    for name in Fishes.definitive().keys():
        if name not in banned:
            valid.append(name)
    return valid


def get_valid_modifiers():
    """Return a list of valid modifier wishes."""

    valid = []
    banned = ['Default']

    for name in Modifiers.definitive().keys():
        if name not in banned:
            valid.append(name)
    return valid


def get_wish_chances(wish_points):
    """Get wish chances."""

    fish_chance = 100 if wish_points >= 100 else wish_points
    wish_points -= fish_chance

    first_mod_chance = 100 if wish_points >= 100 else wish_points
    wish_points -= first_mod_chance

    second_mod_chance = 100 if wish_points >= 100 else wish_points
    wish_points -= second_mod_chance

    third_mod_chance = 100 if wish_points >= 100 else wish_points
    wish_points -= third_mod_chance

    return fish_chance, first_mod_chance, second_mod_chance, third_mod_chance


def show_wish_data():
    """Show information related to wishing if the player has wish points."""

    state = load_state()

    if state.wish_points > 0:
        show_detailed_wish_data(state.wish_points)
    else:
        log_text('You should find a star to wish upon first.')

    save_state(state)


def do_wish(fish_index=-1, first_mod_index=-1, second_mod_index=-1, third_mod_index=-1):
    """Perform a wish."""

    state = load_state()

    if state.wish_points > 0:
        cost = 0

        try:
            if fish_index >= 0:
                fish = get_valid_fishes()[fish_index]
                cost += 100
            else:
                return invalid_wish()
        except (ValueError, IndexError):
            return invalid_wish()

        try:
            if first_mod_index >= 0:
                first_mod = get_valid_modifiers()[first_mod_index]
                cost += 100
            else:
                first_mod = Modifiers.definitive().get_random_value().name
        except (ValueError, IndexError):
            return invalid_wish()

        try:
            if second_mod_index >= 0:
                second_mod = get_valid_modifiers()[second_mod_index]
                cost += 100
            else:
                second_mod = ''
        except (ValueError, IndexError):
            return invalid_wish()

        try:
            if third_mod_index >= 0:
                third_mod = get_valid_modifiers()[third_mod_index]
                cost += 100
            else:
                third_mod = ''
        except (ValueError, IndexError):
            return invalid_wish()

        fish_chance, first_mod_chance, second_mod_chance, third_mod_chance = (
            get_wish_chances(state.wish_points)
        )

        set_next_fish_data(
            state,
            try_wish(fish, fish_chance, Fishes.definitive()),
            try_wish(first_mod, first_mod_chance, Modifiers.definitive()),
            try_wish(second_mod, second_mod_chance, ''),
            try_wish(third_mod, third_mod_chance, '')
        )

        log_text('You\'ve made a wish!')
        state.wish_points -= cost
        if state.wish_points < 0:
            state.wish_points = 0
    else:
        log_text('You should find a star to wish upon first.')

    save_state(state)


def try_wish(desired, chance, fail_pool):
    """Return desired value if the chance succeeds, return a value from the fail pool otherwise."""

    roll = random.randrange(0, 100)
    if roll < chance:
        return desired
    else:
        try:
            return fail_pool.get_random_value().name
        except AttributeError:
            return fail_pool


def invalid_wish():
    """Inform the fisher of an invalid wish."""

    log_text('Invalid wish!')
    return False


def show_detailed_wish_data(wish_points):
    """Show information related to wishing."""

    fish_chance, first_mod_chance, second_mod_chance, third_mod_chance = (
        get_wish_chances(wish_points)
    )

    log_text('Fishes:')
    index = 0
    for valid in get_valid_fishes():
        log_text(f'[{index}] {valid}')
        index += 1

    log_text('\nModifiers:')
    index = 0
    for valid in get_valid_modifiers():
        log_text(f'[{index}] {valid}')
        index += 1

    log_text(f'\nWish Points: {wish_points}\n')
    log_text(f'Chance for Fish: {percentage(fish_chance)}')
    log_text(f'Chance for First Modifier: {percentage(first_mod_chance)}')
    log_text(f'Chance for Second Modifier: {percentage(second_mod_chance)}')
    log_text(f'Chance for Third Modifier: {percentage(third_mod_chance)}')
