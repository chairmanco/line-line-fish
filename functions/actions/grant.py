from functions.items.item_utils import add_item_by_name
from functions.state.game_state import load_state, save_state
from functions.text.formatting import dollars
from functions.text.logging import log_text
from objects.info.item_type import ItemType


def grant_item(item, qty=1, sell_price=1, buy_price=1):
    """Grant items to the boat."""

    item = item.replace('_', ' ')
    state = load_state()

    if not add_item_by_name(state, item, qty):
        state.inventory.add_item(
            item,
            qty,
            {
                'use_type': ItemType.NO_TYPE,
                'buy': buy_price,
                'sell': sell_price,
                'text': f'Custom item with no description\n\n\"Custom item flavour text.\"'
            }
        )

    log_text(f'Granted {item} x {qty}.')

    save_state(state)


def grant_money(quantity=3141592653):
    """Grant money to the fisher."""

    state = load_state()
    state.money += quantity

    log_text(f'Granted {dollars(quantity)}.')
    log_text(f'New total: {dollars(state.money)}.')
    save_state(state)


def grant_tokens(quantity=3141592653):
    """Grant time tokens to the fisher."""

    state = load_state()
    state.time_tokens.tokens += quantity

    if quantity == 1:
        log_text(f'Granted {quantity} fishing line token.')
    else:
        log_text(f'Granted {quantity} fishing line tokens.')
    log_text(f'New total: {state.time_tokens.add_tokens()}.')
    save_state(state)
