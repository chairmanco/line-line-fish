from functions.logic.special_catches.relics import get_relic_data
from functions.text.conditional_text import cond_string
from functions.text.formatting import percentage, kilograms, dollars
from functions.text.logging import log_text


def show_info(state):
    """Show information related to the current fisherman."""

    catch_rates = list(state.history.values())
    most_frequent = list(state.history.keys())[catch_rates.index(max(catch_rates))]
    highest_rate = state.history[most_frequent]
    ws_percent = percentage(state.whale_shark, state.weight)

    log_text(f'\nYou have caught {state.caught} oceanic objects.')
    log_text(f'Total Catch: {kilograms(state.weight)} '
             + cond_string(f'({ws_percent} Whale Shark)', state.whale_shark))

    log_text(f'\nTreasure Chests: {state.chests}')
    log_text(f'Iridescent Catches: {state.iridescent}')
    log_text(f'{get_relic_data(state)[1]}/{get_relic_data(state)[2]} Legendary Relics')

    log_text(f'\nMost Frequent Catch: {most_frequent} ({highest_rate} caught)')
    log_text(f'Biggest Catch: {state.biggest.name},'
             + f' {kilograms(state.biggest.weight)},'
             + f' worth {dollars(state.biggest.value)}')
    log_text(f'Most Valuable Catch: {state.prized.name},'
             + f' {kilograms(state.prized.weight)},'
             + f' worth {dollars(state.prized.value)}')
    if state.biggest_net[1] > 0:
        log_text(f'Most Valuable Net: {dollars(state.biggest_net[0])}'
                 f' from a net of {state.biggest_net[1]} catches')
    if len(state.newborns):
        captured = ''
        for newborn in state.newborns:
            captured = f'{captured}, {newborn}'
        log_text(f'\nNewborns Captured: {captured[2:]}')
