from functions.text.logging import log_text


def cond_string(text, condition, newline=False):
    """Formats text conditionally with a conditional newline."""

    output = ''
    if newline:
        output += '\n'
    if condition:
        output += text

    return output


def cond_print(text, condition, newline=False):
    """Prints text conditionally with a conditional newline."""

    if newline:
        log_text()
    if condition:
        log_text(text)
