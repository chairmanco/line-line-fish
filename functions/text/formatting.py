from functions.text.conditional_text import cond_string


def a_an(text, caps=False):
    if not text:
        return ''
    if text[0] in ('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'):
        return 'An' if caps else 'an'
    return 'A' if caps else 'a'


def percentage(numerator, denominator=100):
    """Format a percentage with error handling."""

    try:
        return f'{format(numerator / denominator * 100, ".2f")}%'
    except ZeroDivisionError:
        return '0.00%'


def kilograms(value):
    """Format a value as kilograms."""

    return f'{format(value, ".2f")} kg'


def dollars(value, show_plus=False):
    """Format a string as dollars."""

    return (f'{cond_string(cond_string("+", show_plus) + "$", value > 0)}'
            f'{cond_string(cond_string("-", show_plus or value < 0) + "$", value <= 0)}'
            f'{format(abs(value), ".2f")}')


def roman(integer):
    """Return the Roman numeral text of an integer.

    Source: https://stackoverflow.com/a/42875240
    """

    numbers = (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1)
    symbols = ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
    roman_num = ""

    for i in range(len(numbers)):
        count = int(integer / numbers[i])
        roman_num += symbols[i] * count
        integer -= numbers[i] * count

    return roman_num
