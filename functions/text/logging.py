def log_text(text=None):
    """Send text to the logger."""

    if text:
        logger.log(text)
    else:
        logger.log()


def print_log():
    """Print the logger's contents."""

    for line in logger.get_log():
        print(line)


def get_log():
    """Get the logger's contents."""

    return logger.get_log()


def clear_log():
    """Clear the logger's contents."""

    return logger.clear_log()


class TextLog:
    """Stores text logged."""

    def __init__(self):
        self.text_log = []

    def log(self, text=None):
        """Store lines of text."""

        if text:
            tokens = text.split('\n')
            for token in tokens:
                self.text_log.append(token)
        else:
            self.text_log.append('')

    def get_log(self):
        """Return all lines of text stored."""

        return self.text_log

    def clear_log(self):
        """Delete all lines of text stored."""

        self.text_log = []


logger = TextLog()
