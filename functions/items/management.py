from functions.items.display import show_store, show_store_item, show_inventory, show_inventory_item
from functions.items.exchange import sink_item, sell_item, buy_item
from functions.text.conditional_text import cond_print
from functions.text.logging import log_text


def manage_items(boat, store, state, item_index, exchange_qty):
    """View, buy, or sell items from the store or boat."""

    if (store or boat) and item_index <= 0 < exchange_qty:
        log_text('\nYou need to specify an item.')

    if store:
        manage_store_items(state, item_index, exchange_qty, boat)

    if boat:
        manage_boat_items(state, item_index, exchange_qty, store)

    if store and exchange_qty < 0:
        log_text('\nOnly positive quantities of items may be exchanged.')


def manage_store_items(state, item_index, exchange_qty, boat):
    """View or buy items from the store."""

    if item_index > 0 and exchange_qty > 0:
        log_text()
        buy_item(state, item_index, exchange_qty)

    if item_index <= 0 and not exchange_qty:
        log_text('\nStore:')
        show_store()

    if item_index > 0 and not exchange_qty:
        log_text()
        cond_print(f'Store Item {item_index}:', boat)
        show_store_item(item_index)


def manage_boat_items(state, item_index, exchange_qty, store):
    """View or sell items from the boat."""

    if item_index > 0 and exchange_qty > 0:
        log_text()
        sell_item(state, item_index, exchange_qty)

    if item_index > 0 > exchange_qty:
        log_text()
        sink_item(state, item_index, -exchange_qty)

    if item_index <= 0 and not exchange_qty:
        log_text('\nBoat:')
        show_inventory(state.inventory, state.ship_size)

    if item_index > 0 and not exchange_qty:
        log_text()
        cond_print(f'-----\n\nBoat Item {item_index}:', store)
        show_inventory_item(state.inventory, item_index, state.feat_book, state.recipe_book)
