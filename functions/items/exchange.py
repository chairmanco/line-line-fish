import math

from functions.items.item_utils import decrement_item_by_name, add_item_into_sea
from functions.text.formatting import dollars
from functions.text.logging import log_text
from objects.data.items.inventory import Inventory


def sink_item(state, item_index, exchange_qty):
    """Sink an item from the boat."""

    item = state.inventory.get(state.inventory.nth_key(item_index - 1))

    if item:
        value = item.get_sell_price()
        log_text(f'{item.name} - Value: {dollars(value)}')
        if exchange_qty > item.qty:
            log_text(f'Quantity Owned: {item.qty}')
            exchange_qty = item.qty

        decrement_item_by_name(state, item.name, exchange_qty)
        add_item_into_sea(state, item, exchange_qty)

        log_text(f'Sunk {item.name} x {exchange_qty} into the sea.')
        if item.contains('Poseidon\'s Laptop'):
            log_text(f'\nThe sea rumbles, then suddenly calms.'
                     f'\nYou hear the faint sound of fingers typing from the deep.')
            state.favour = True
            state.feat_book.toggle_feat(10, state)
    else:
        log_text(f'No boat item at index: {item_index}')


def sell_item(state, item_index, exchange_qty):
    """Sell an item from the boat."""

    item = state.inventory.get(state.inventory.nth_key(item_index - 1))

    if item:
        value = item.get_sell_price()
        log_text(f'{item.name} - Value: {dollars(value)}')
        if exchange_qty > item.qty:
            log_text(f'Quantity Owned: {item.qty}')
            exchange_qty = item.qty
        profit = exchange_qty * value
        decrement_item_by_name(state, item.name, exchange_qty)
        state.money += profit
        log_text(f'Sold {item.name} x {exchange_qty} for {dollars(profit)}.')
        log_text(f'Total Funds: {dollars(state.money)}')
    else:
        log_text(f'No boat item at index: {item_index}')


def buy_item(state, item_index, exchange_qty):
    """Buy an item from the store."""

    item = Inventory.for_sale().get(Inventory.for_sale().nth_key(item_index - 1))

    if item:
        log_text(f'{item.name} - Cost: {dollars(item.buy)}')
        if exchange_qty * item.buy > state.money:
            exchange_qty = math.floor(state.money / item.buy)
            log_text(f'Affordable quantity: {exchange_qty}')
        cost = exchange_qty * item.buy
        state.inventory.add_item_item(item, exchange_qty)
        item.qty += exchange_qty
        state.money -= cost
        log_text(f'Bought {item.name} x {exchange_qty} for {dollars(cost)}.')
        log_text(f'Total Funds: {dollars(state.money)}')
    else:
        log_text(f'No store item at index: {item_index}')
