from functions.text.logging import log_text
from objects.data.items.inventory import Inventory
from objects.data.sea_objects.item_catch import ItemCatch


def is_overloaded(inventory, size):
    """Return number of excess items iff an inventory contains more items than size."""

    item_count = 0
    for index in range(len(inventory)):
        item = inventory.get(inventory.nth_key(index))
        item_count += item.qty

    return item_count - size if item_count > size else 0


def get_item_of_name(name):
    """Get a default instance of an item by its name."""

    return Inventory.definitive().get(name)


def add_item_by_name(state, name, qty=1):
    """Add an item to the boat from the main list of items."""

    if Inventory.definitive().get(name):
        state.inventory.add_item_item(Inventory.definitive().get(name), qty)
        return True
    else:
        return False


def add_item_from_catch(state, catch):
    """Retrieve a fished sunken item."""

    if isinstance(catch, ItemCatch):
        state.inventory.add_item_item(catch.get_item())
        log_text(f'You added the {catch} to your boat.\n')


def add_item_into_sea(state, item, qty=1):
    """Add sunken items into the sea."""

    state.persistent_sea.add_sunken_item(item.clone(), qty)


def decrement_item_by_name(state, name, qty=1):
    """Decrement an item in the boat."""

    state.inventory.decrement_item(name, qty)


def get_qty_of_name(state, name):
    """Get the quantity of an item in the boat."""

    item = state.inventory.get(name)
    if item:
        return item.qty
    else:
        return 0
