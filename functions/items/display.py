from interface.config import config
from functions.text.conditional_text import cond_string, cond_print
from functions.text.formatting import dollars
from functions.text.logging import log_text
from objects.data.items.inventory import Inventory
from objects.info.item_type import use_type_string

NAME_PADDING_MAX = config.get('NAME_PADDING_MAX')


def show_inventory_item(inventory, item_index, feat_book, recipe_book):
    """Display the selected store item."""

    item = inventory.get(inventory.nth_key(item_index - 1))

    if item:
        value = item.get_sell_price()
        log_text(f'{item.name}' + cond_string(f'; Qty: {item.qty}', item.qty > 1))
        log_text(f'Value: {dollars(value)}')
        log_text(f'Type: {use_type_string(item.use_type)}')
        log_text(f'\nDescription: {item.text}')
        if item.contains('Feats of Fishing'):
            show_feats_of_fishing(feat_book)
        if item.contains('Crafting for Dummies'):
            show_recipe_book(recipe_book)
    else:
        log_text(f'No boat item at index: {item_index}')


def show_feats_of_fishing(feat_book):
    log_text(f'\nFeats Achieved ({len(feat_book.feats)} Total):')
    for i in range(len(feat_book.feats)):
        if feat_book.feat_list[i]:
            log_text(f'{feat_book.feats[i]}')


def show_recipe_book(recipe_book):
    log_text(f'\nRecipes Known:')
    for i in range(len(recipe_book.get_all_recipes())):
        if recipe_book.recipe_list[i]:
            log_text(f'[{i}] {recipe_book.get_all_recipes()[i]}\n')


def show_store_item(item_index):
    """Display the selected store item."""

    item = Inventory.for_sale().get(Inventory.for_sale().nth_key(item_index - 1))

    if item:
        log_text(item.name)
        log_text(f'Cost: {dollars(item.buy)}')
        log_text(f'Type: {use_type_string(item.use_type)}')
        log_text(f'\nDescription: {item.text}')
        # If a item is exchanged but store/boat is ambiguous, try assuming both
    else:
        log_text(f'No store item at index: {item_index}')


def show_inventory(inventory, ship_size):
    """Display the contents of an inventory."""

    item_count = 0
    for index in range(len(inventory)):
        item = inventory.get(inventory.nth_key(index))
        value = item.get_sell_price()
        log_text(f'[{index + 1}]: {item.name}'
                 f'{" " * (NAME_PADDING_MAX - len(item.name) + (index + 1 < 10))}'
                 f' -  Value: {dollars(value)}'
                 + cond_string(f' x {item.qty}', item.qty > 1))
        item_count += item.qty

    log_text(f'\nStorage Space: {item_count}/{ship_size} spots filled')


def show_store():
    """Display the contents of the store."""

    store = Inventory.for_sale()

    for index in range(len(store)):
        item = store.get(store.nth_key(index))
        cond_print(f'[{index + 1}]: {item.name}'
                   f'{" " * (NAME_PADDING_MAX - len(item.name) + (index + 1 < 10))}'
                   f' -  {dollars(item.buy)}', item.buy)
