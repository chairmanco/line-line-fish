import random

from functions.items.item_utils import decrement_item_by_name
from functions.text.formatting import dollars
from functions.text.logging import log_text


def use_consumable_single(state):
    """Use all consumables which are consumed once per catch cycle."""

    newline = False

    item = state.inventory.get('Admiral Burger Combo')
    if item:
        log_text(f'You threw an Admiral Burger Combo into the sea. ({item.qty - 1} remaining)')
        state.burger = True
        decrement_item_by_name(state, item.name)
        newline = True

    item = state.inventory.get('Colour Enhancing Flakes')
    if item:
        log_text(f'You threw Colour Enhancing Flakes into the sea. ({item.qty - 1} remaining)')
        state.flakes = True
        decrement_item_by_name(state, item.name)
        newline = True

    if newline:
        log_text()


def use_tickets(ticket, state):
    """Use all scratch tickets."""

    winnings = 0
    winners = 0
    for i in range(ticket.qty):
        lottery = random.randrange(0, 1000000)
        if lottery < 1 + state.luck:
            winnings += 1000000
            winners += 1
    if ticket.qty > 1:
        log_text(f'You scratched {ticket.qty} scratch tickets.')
    else:
        log_text('You scratched a scratch ticket.')
    if winnings:
        if winners > 1:
            log_text(f'You have won {dollars(winnings)} from {winners} winning tickets!')
        else:
            log_text(f'You have won {dollars(winnings)}!')
        state.money += winnings
    else:
        log_text('You did not win any money.')
    state.inventory.remove_item(ticket.name)


def use_consumable_all(state):
    """Use all consumables which are consumed in entirety when used."""

    newline = False

    item = state.inventory.get('Lucky Sea Hare\'s Foot')
    if item:
        log_text(f'Your luck has increased{"!" * item.qty}')
        state.luck += item.qty
        state.inventory.remove_item(item.name)
        newline = True

    item = state.inventory.get('Bait Shop Coupon')
    if item:
        log_text(f'Your bait discount has increased by {dollars(item.qty / 2.0)}.')
        state.discount += item.qty / 2.0
        state.inventory.remove_item(item.name)
        newline = True

    item = state.inventory.get('DIY Shipbuilding Kit')
    if item:
        log_text(f'Your boat\'s storage space has increased by {item.qty * 10}.')
        state.ship_size += item.qty * 10
        state.inventory.remove_item(item.name)
        newline = True

    item = state.inventory.get('Scratch Ticket')
    if item:
        use_tickets(item, state)
        newline = True

    item = state.inventory.get('Recipe Fortune Cookie')
    if item:
        if item.qty > 1:
            log_text(f'You opened {item.qty} Recipe Fortune Cookies.\n')
        else:
            log_text('You opened a Recipe Fortune Cookie.\n')
        for i in range(item.qty):
            if not state.recipe_book.toggle_unknown_recipe():
                state.recipe_book.excess_wisdom += item.qty - i - 1
                break
        state.inventory.remove_item(item.name)

    if newline:
        log_text()
