from functions.text.formatting import dollars
from functions.text.logging import log_text


def process_turtle(state, catch):
    """Handle effects caused by turtles."""

    if (catch.contains('Endangered Sea Turtle')
            and not catch.contains('No-Longer-Endangered')
            and not state.inventory.get('Turtle Conservation Papers')):
        fine = state.money * 0.15
        log_text('As penalty for catching an endangered species, you have been fined!')
        log_text(f'The fine is {dollars(fine)}, 15% of your total funds.\n')
        state.money -= fine
