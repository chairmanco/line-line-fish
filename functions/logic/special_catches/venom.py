import math
import random

from functions.items.item_utils import decrement_item_by_name
from functions.logic.death import fisherman_death
from functions.text.logging import log_text


def process_venom(state, catch, warded):
    """Handle effects caused by venom."""

    if state.inventory.get('Anti-Venom'):
        cure_venom(state)

    if state.venom:
        do_venom_effect(state)

    if catch.contains('Lion\'s Mane Jelly') or catch.contains('Sea Snake'):
        apply_venom_from_catch(state, catch, warded)


def cure_venom(state):
    """Cure the effects of venom."""

    decrement_item_by_name(state, 'Anti-Venom')
    log_text(f'You applied an Anti-Venom to cure your poisoning.\n')


def do_venom_effect(state):
    """Perform the effects of venom, including curing."""

    if not random.randrange(0, 10):
        log_text(f'You succumbed to the effects of your poisoning!\n')
        fisherman_death(state)
    else:
        state.venom -= 1
        log_text(f'{state.venom} ml of venom still lingers in your veins.\n')


def apply_venom_from_catch(state, catch, warded):
    """Attempt to apply venom from a catch."""

    if warded:
        log_text(f'The {catch.name} threatened to poison you, but was warded off by the sound'
                 f' of a crying lady.\n')
    else:
        potency = math.ceil(catch.value / 10)
        log_text(f'The {catch.name} injected you with {potency} ml of venom!\n')
        state.venom += potency
