import random

from functions.items.item_utils import decrement_item_by_name
from functions.logic.death import fisherman_death
from functions.text.conditional_text import cond_print
from functions.text.formatting import dollars
from functions.text.logging import log_text
from objects.data.sea_objects.full_catch import FullCatch


def process_shark(state, catch):
    """Handle effects caused by sharks."""

    if isinstance(catch, FullCatch) and catch.captured:
        return True

    if catch.contains('Shark\'thulu'):
        return process_thulu(state, catch)
    elif catch.contains('Shark') and not catch.contains('Whale'):
        harpoon = state.inventory.get('Legendary Violent Harpoon')
        shark_stopped = defeat_shark(state, catch.name) and any(
            ('Frenzied' not in catch.name, harpoon)
        )
        consumed = int(not any((state.mermaid, shark_stopped)))

        cond_print(f'You disemboweled the {catch.name} with your harpoon,'
                   f' claiming its viscera as bounty.',
                   all((catch.contains('Bull Shark'), harpoon)))
        cond_print(f'You impaled the {catch.name} with a trident, yielding its flesh as bounty.',
                   shark_stopped and not harpoon)
        cond_print(f'You impaled the {catch.name} with a trident, but not before it attacked in'
                   ' a wild frenzy.',
                   all((defeat_shark(state, catch.name), 'Frenzied' in catch.name, not harpoon)))

        cond_print(f'Your {state.mermaid}\'s blessing was eaten by a {catch}.',
                   state.mermaid and not shark_stopped)
        cond_print(f'Your Puppy in a Raincoat was sent to away to play at a nice farm.  Forever.',
                   state.inventory.get('Puppy in a Raincoat') and not shark_stopped)

        cond_print(f'You were eaten by a {catch}.'
                   f'  Your {dollars(state.money)} were eaten too.', consumed)
        log_text()

        if not shark_stopped:
            state.mermaid = ''

            item = state.inventory.get('Puppy in a Raincoat')
            if item:
                decrement_item_by_name(state, item.name)

            if consumed:
                fisherman_death(state)

        return shark_stopped
    return False


def process_thulu(state, catch):
    """Handle effects caused by Old Gods."""

    if catch.contains('Shark\'thulu'):
        shark_stopped = defeat_thulu(state) and 'Frenzied' not in catch.name
        consumed = int(not shark_stopped)

        cond_print(f'You slew {catch.name}, yielding the titan\'s flesh as bounty!',
                   shark_stopped)
        cond_print(f'You and {catch.name}, mad with rage, slew one another in glorious combat!',
                   defeat_thulu(state) and 'Frenzied' in catch.name)

        cond_print(f'Your {state.mermaid}\'s blessing was devoured by {catch}.',
                   state.mermaid and not shark_stopped)
        cond_print(f'Your Puppy in a Raincoat was lovingly devoured by {catch.name}.',
                   state.inventory.get('Puppy in a Raincoat') and not shark_stopped)

        cond_print(f'You were devoured by {catch}.'
                   f'  Your {dollars(state.money)} were devoured too.', consumed)
        cond_print(f'\nNo blessing can ward against this dreadful creature.\n',
                   not shark_stopped)
        cond_print(f'\nNo blessing can ward against this dreadful creature,'
                   f' but apparently you can.\n',
                   shark_stopped)

        if defeat_thulu(state):
            state.feat_book.toggle_feat(6, state)

        if not shark_stopped:
            state.mermaid = ''

            item = state.inventory.get('Puppy in a Raincoat')
            if item:
                decrement_item_by_name(state, item.name)

            if consumed:
                fisherman_death(state)

        return shark_stopped
    return False


def set_anti_shark_power(state):
    """Choose random number to compare against shark-killing ability."""

    state.anti_shark = random.randrange(0, 100)


def defeat_shark(state, shark_name):
    """Return true iff the shark has been defeated."""

    item = state.inventory.get('War Trident')
    if item:
        tridents = item.qty
    else:
        return False

    if 'Bull' in shark_name:
        return state.inventory.get('Legendary Violent Harpoon') or state.anti_shark < tridents * 25
    if 'Great White' in shark_name:
        return state.anti_shark < tridents * 10


def defeat_thulu(state):
    """Return true iff the Old God of the sea has been defeated."""

    item = state.inventory.get('Shark\'thulu\'s Bane')
    if item:
        banes = item.qty
    else:
        return False

    return state.anti_shark < banes
