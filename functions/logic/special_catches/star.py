import math

from functions.text.logging import log_text


def process_star(state, catch):
    """Handle effects caused by stars."""

    if catch.contains('Brittle Star'):
        add_wish_points(state, catch)


def add_wish_points(state, catch):
    """Add wish points from a Brittle Star"""

    points = math.ceil(catch.weight)
    log_text(f'It is always wise to wish upon a star.')
    log_text(f'You keep the {catch.name} in mind, knowing it is worth {points}% of a wish.\n')
    state.wish_points += points
