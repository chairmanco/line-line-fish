import random

from functions.items.item_utils import decrement_item_by_name
from functions.text.formatting import dollars
from functions.text.logging import log_text
from objects.data.items.inventory import Inventory
from objects.data.sea_objects.full_catch import FullCatch


def kraken_gift(state, item):
    """Trick the Kraken with poison if applicable."""

    if item.contains('Krakengift'):
        log_text(f'You hear a deafening roar from the ocean.')
        log_text(f'The Kraken of Grand Larceny lives, though it will not keep its bounty.')
        log_text(f'Thirteen items rise from the sea before you:')
        for i in range(0, 13):
            new_item = random.choice(list(Inventory.definitive().values()))
            state.inventory.add_item_item(new_item)
            log_text(f'* {new_item.name}\n')
        state.feat_book.toggle_feat(7, state)


def process_mollusk(state, catch):
    """Handle effects caused by mollusks."""

    if isinstance(catch, FullCatch) and catch.captured:
        return True

    if catch.count('Octopus') and not state.inventory.get('Hungry Guard Seal'):
        item = state.inventory.get('Fake Bait Shop Coupon')
        if item:
            log_text('The octopus reaches into your pocket and steals a Fake Bait Shop Coupon!\n')
            state.inventory.remove_item(item.name)
        else:
            if state.discount:
                log_text('The octopus reaches into your pocket and steals your discount card!')
                log_text(f'Your bait discount has been reset to {dollars(0)}.\n')
                state.discount = 0
            elif state.money > catch.value:
                log_text('The octopus reaches into your pocket and steals money from your wallet!')
                stolen = max(round(state.money / 4, 2), 1000)
                stolen = min(state.money, stolen)
                log_text(f'The octopus gets away with {dollars(stolen)}!\n')
                state.money -= stolen
            else:
                log_text('The octopus reaches into your pocket and finds that you are penniless.')
                log_text(f'In an act of pity, it hands you {dollars(200)}.\n')
                state.money += 200

    if catch.count('Kraken') and not state.inventory.get('Hungry Guard Seal'):
        item = state.inventory.get_random_value()
        if item:
            log_text(f'The Kraken of Grand Larceny wraps its tentacles around your {item.name}!')
            log_text('It disappears, menacingly, back into the abyss.\n')
            decrement_item_by_name(state, item.name, 1)
            kraken_gift(state, item)
        else:
            log_text(f'The Kraken of Grand Larceny sinks away, having given you mercy for now.\n')
