from functions.items.item_utils import decrement_item_by_name
from functions.text.logging import log_text
from objects.info.item_type import ItemType


def process_mermaid(state, catch):
    """Handle effects caused by mermaid catches."""

    if catch.contains('Mermaid'):
        if state.mermaid:
            log_text(f'The {catch} is enticed to join you on your yacht.\n')
            state.inventory.add_item(
                catch.name,
                1,
                {
                    'use_type': ItemType.NO_TYPE,
                    'buy': 0,
                    'sell': catch.value,
                    'text': f'A magical sea creature that can protect you from harm'
                    '\n\n"Look at the big shot over here, partying with mermaids on a yacht."',
                }
            )
        else:
            log_text(f'You are now protected by the {catch}\'s blessing.\n')
            state.mermaid = catch.name


def apply_mermaid(state):
    """Apply a new mermaid's blessing if possible."""

    if not state.mermaid:
        mermaids = {}

        for name, item in state.inventory.items():
            if 'Mermaid' in name and 'Exhausted' not in name:
                value = item.get_sell_price()
                mermaids[name] = value

        if len(mermaids):
            costs = list(mermaids.values())
            cheapest = list(mermaids.keys())[costs.index(min(costs))]
            chosen_mermaid = state.inventory.get(cheapest)
            log_text(f'Your {chosen_mermaid.name} has blessed you with protection.\n')
            state.mermaid = chosen_mermaid.name
            value = min(costs)
            decrement_item_by_name(state, chosen_mermaid.name)
            state.inventory.add_item(
                f'Exhausted {chosen_mermaid.name}',
                1,
                {
                    'use_type': ItemType.NO_TYPE,
                    'buy': 0,
                    'sell': value / 10,
                    'text': f'A magical sea creature which has spent its blessing'
                    '\n\n"Selling used mermaids is as cruel as it is efficient."',
                }
            )
