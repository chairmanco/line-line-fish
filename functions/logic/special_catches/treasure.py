from functions.text.conditional_text import cond_string
from functions.text.logging import log_text


def process_treasure(state, catch):
    """Handle effects related to treasure."""

    state.chests += catch.count('Chest')

    if state.inventory.get('Fisherman\'s Magnet'):
        if 'Treasure' in catch.name or 'Legendary' in catch.name:
            state.treasure_misses = 0
            state.treasure_boost = 0
        else:
            magnet = ('Fisherman\'s Magnets are'
                      if state.inventory.get('Fisherman\'s Magnet').qty > 1 else
                      'Fisherman\'s Magnet is')
            state.treasure_misses += 1
            if state.treasure_misses >= 20:
                state.treasure_misses = 0
                state.treasure_boost += state.inventory.get('Fisherman\'s Magnet').qty
                unit = f'lumen{cond_string("s", state.treasure_boost > 1)}'
                log_text(
                    f'Your {magnet} glowing brightly at {state.treasure_boost} {unit}.\n'
                )
            if state.treasure_boost >= 10:
                log_text(f'Your {magnet} emitting an electromagnetic pulse!\n')
                state.persistent_sea.increase_frequency(
                    'Lion\'s Mane Jelly', state.treasure_boost
                )
