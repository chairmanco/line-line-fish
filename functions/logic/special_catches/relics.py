import random

from functions.text.logging import log_text
from objects.data.items.inventory import Inventory
from objects.info.item_info import all_items
from objects.info.item_type import ItemType


def get_relic_data(state):
    """Return the list of relics which can be caught, and the number of unique owned vs total."""

    relic_list = Inventory()
    for name, data in all_items.items():
        if data['use_type'] == ItemType.PERSISTENT_RELIC:
            relic_list.add_item_dict(name, data)

    unowned_list = Inventory()
    for item in relic_list.values():
        if not state.inventory.get(item.name):
            unowned_list.add_item_item(item)

    relics = len(relic_list)
    owned = relics - len(unowned_list)

    if len(unowned_list):
        return unowned_list, owned, relics
    else:
        return relic_list, owned, relics


def process_relics(state, catch):
    """Handle effects related to catching relics."""

    if 'Legendary' in catch.name:
        relic = random.choice(list(get_relic_data(state)[0].values()))
        log_text(f'You found the {relic.name}!\n')
        state.inventory.add_item_item(relic)
