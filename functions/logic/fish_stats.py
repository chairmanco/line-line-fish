from functions.items.item_utils import decrement_item_by_name
from functions.text.logging import log_text
from objects.data.fish_data.catch_data import CatchData
from objects.info.item_type import ItemType


def process_weight_value(state, catch):
    """Handle effects caused by catch weight and value."""

    if catch.weight >= 2000:
        item = state.inventory.get('Lobster Cage')
        if item:
            log_text(f'Your Lobster Cage has buckled under the weight of your catch!')
            decrement_item_by_name(state, 'Lobster Cage')
            state.inventory.add_item(
                f'Buckled Lobster Cage',
                1,
                {
                    'use_type': ItemType.NO_TYPE,
                    'buy': 0,
                    'sell': item.buy / 20,
                    'text': f'Once allowed you to catch Lobsters, now only catches only rust and'
                    ' sorrow\n\n"Usable as scrap metal or as an abstract art piece."'
                }
            )

    if isinstance(catch, CatchData):
        if catch.weight > state.biggest.weight:
            state.biggest = catch

        if catch.value > state.prized.value:
            state.prized = catch
