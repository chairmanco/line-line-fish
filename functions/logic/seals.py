from functions.items.item_utils import decrement_item_by_name
from functions.text.conditional_text import cond_string
from functions.text.formatting import kilograms, dollars
from functions.text.logging import log_text
from objects.info.item_type import ItemType


def process_seal(state, catch):
    """Handle effects caused by seals."""

    seal_fed = eat_special_prey(state, catch)

    return feed_seals(state, catch, seal_fed)


def feed_seals(state, catch, special_prey_eaten):
    """Feed seals using a combination of catches and seal chow."""

    guards = state.inventory.get('Hungry Guard Seal')
    guards = guards.qty if guards else 0
    veterans = state.inventory.get('Injured Guard Seal Veteran')
    veterans = veterans.qty if veterans else 0
    total_seals = guards + veterans

    full_from_prey = 1 * special_prey_eaten
    chow = state.inventory.get('Seal Chow')
    initial_chow = chow.qty if chow else 0

    consumed = 0
    seal_message = ''

    full_from_prey, guards = reduce_equally(full_from_prey, guards)
    full_from_prey, veterans = reduce_equally(full_from_prey, veterans)

    new_chow, guards = reduce_equally(initial_chow, guards)
    new_chow, veterans = reduce_equally(new_chow, veterans)

    if new_chow < initial_chow:
        eaten_chow = initial_chow - new_chow
        decrement_item_by_name(state, 'Seal Chow', eaten_chow)
        if total_seals > 1:
            seal_message = f'Your seals ate {eaten_chow} units of Seal Chow.\n'
        else:
            seal_message = f'Your seal ate {eaten_chow} unit of Seal Chow.\n'

    unfed_seals = guards + veterans
    if unfed_seals > 0:
        consumed = guards * 10 + veterans * 5
        consumed = consumed * (consumed < 100) + 100 * (consumed >= 100)
        if unfed_seals > 1:
            seal_message = f'{seal_message}Your unfed seals eat {consumed}% of your catch weight.'
        else:
            seal_message = f'{seal_message}Your unfed seal eats {consumed}% of your catch weight.'

    catch.weight = catch.weight * (100 - consumed) / 100
    catch.value = catch.value * (100 - consumed) / 100
    consumed_message = cond_string(f'Your catch has been reduced to {kilograms(catch.weight)},'
                                   f' worth {dollars(catch.value)}.', consumed)
    return cond_string(f'{seal_message}\n{consumed_message}\n', seal_message)


def eat_special_prey(state, catch):
    """Handle seals consuming birds or mollusks and returning whether a feeding has occurred."""

    seal_ate_special = False

    if catch.count('Octopus') and state.inventory.get('Hungry Guard Seal'):
        log_text('The octopus is savagely eaten by your Hungry Guard Seal before it can rob you!\n')
        seal_ate_special = True

    if catch.count('Kraken') and state.inventory.get('Hungry Guard Seal'):
        log_text(f'Your Hungry Guard Seal bites the Kraken of Grand Larceny before it can rob you!')
        log_text(f'In retaliation, the Kraken of Grand Larceny wounds your Hungry Guard Seal!\n')
        state.inventory.add_item(
            f'Injured Guard Seal Veteran',
            1,
            {
                'use_type': ItemType.NO_TYPE,
                'buy': 0,
                'sell': state.inventory.get('Hungry Guard Seal').buy / 20,
                'text': f'Eats 5% of your catch weight'
                '\n\n"Soon this dying warrior will eat 5% of your catch weight in Valhalla."'
            }
        )
        decrement_item_by_name(state, 'Hungry Guard Seal')
        seal_ate_special = True

    if catch.contains('Albatross') and state.inventory.get('Hungry Guard Seal'):
        log_text('Your Hungry Guard Seal eats the Albatross, absolving you of its curse!\n')
        seal_ate_special = True

    return seal_ate_special


def reduce_equally(qty_a, qty_b):
    """Reduce both quantities equally until one is zero."""

    if qty_a > qty_b:
        qty_a -= qty_b
        qty_b = 0
    else:
        qty_b -= qty_a
        qty_a = 0

    return qty_a, qty_b
