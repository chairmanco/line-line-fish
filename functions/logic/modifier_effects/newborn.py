from functions.items.item_utils import decrement_item_by_name
from functions.text.formatting import kilograms, dollars
from functions.text.logging import log_text
from objects.data.items.live_catch import LiveCatch
from objects.data.sea_objects.full_catch import FullCatch
from objects.info.item_type import ItemType


def process_newborn(state, catch):
    """Handle effects caused by newborns."""

    if catch.contains('Newborn'):
        item = state.inventory.get('Portable Aquarium')
        if item:
            if isinstance(catch, FullCatch):
                catch.captured = True

            log_text(f'You placed the {catch.name} in a Portable Aquarium!')
            log_text(f'You can sell it manually from your boat or keep it for breeding or'
                     f' companionship.\n')
            decrement_item_by_name(state, 'Portable Aquarium')
            state.inventory.add_item_raw(
                LiveCatch(
                    f'Portable Aquarium: {catch.name} -'
                    f' {kilograms(catch.weight)}, {dollars(catch.value)}',
                    1,
                    {
                        'use_type': ItemType.LIVE_CATCH,
                        'buy': 0,
                        'sell': catch.value,
                        'text': f'An adorable baby sea creature kept alive in a portable aquarium'
                        f'\n\n"If you treat the little fella right, one day you can set it free and'
                        f' slaughter it in an epic struggle between man and nature, parent and'
                        f' child."'
                    },
                    catch
                )
            )
            catch.value = 0
            catch.weight = 0

            if catch.parent_species() not in state.newborns:
                state.newborns.append(catch.parent_species())

            if len(state.newborns) >= 10:
                state.feat_book.toggle_feat(12, state)
