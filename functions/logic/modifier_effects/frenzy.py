import math

from functions.logic.sinking import sink_ship
from functions.text.logging import log_text


def process_frenzy(state, catch):
    """Handle effects caused by frenzy."""

    if 'Frenzied' in catch.name:
        if state.inventory.get('Legendary Violent Harpoon'):
            log_text(f'The catch\'s frenzy was almost too much to handle, but you were able to put'
                     f' a stop to it with your harpoon!\n')
        else:
            damage = math.ceil(catch.weight / 100)
            state.ship_size -= damage
            log_text(f'In its wild frenzy, your quarry damaged your vessel!\n')
            if state.ship_size < 1:
                sink_ship(state)
            else:
                log_text(f'Your ship survived, but with its storage size reduced by {damage}.\n')
