def process_wise(state, catch):
    """Handle effects caused by wise catches."""

    if catch.contains('Wise'):
        state.recipe_book.toggle_unknown_recipe()
