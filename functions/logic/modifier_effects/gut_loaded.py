import random

from functions.text.logging import log_text
from objects.data.items.inventory import Inventory


def process_gut_loaded(state, catch):
    """Handle effects caused by gut-loaded catches."""

    if catch.contains('Gut-Loaded'):
        item = state.inventory.get('Gutting Knife')
        if item:
            log_text(f'You used your Gutting Knife to slice open the {catch.name}!')
            new_item = random.choice(list(Inventory.definitive().values()))
            state.inventory.add_item_item(new_item)

            log_text(f'You found: {new_item.name}\n')
