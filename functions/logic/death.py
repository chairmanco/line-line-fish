from functions.text.logging import log_text


def fisherman_death(state):
    """Execute a standard fisherman's death."""

    if state.inventory.get('Legendary Soothing Seahorse'):
        if state.luck < 0:
            state.luck = 0
        if state.discount < 0:
            state.luck = 0
        log_text('A calming neigh washes over your body.')
        log_text('The figure of a seahorse reassures you that you will not lose everything.')
    else:
        state.luck = 0
        state.discount = 0

    state.venom = 0
    state.money = 0
    state.mermaid = ''
