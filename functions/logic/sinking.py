import random

from functions.items.item_utils import add_item_into_sea, is_overloaded
from functions.logic.death import fisherman_death
from functions.text.formatting import percentage
from functions.text.logging import log_text
from objects.data.items.inventory import Inventory
from objects.info.item_info import all_items
from objects.info.item_type import ItemType


def sink_ship(state):
    """Execute a standard ship sinking."""

    log_text(f'Your ship has sunk, and you with it!\n')
    fisherman_death(state)
    log_text()
    items_lost = len(state.inventory)

    for item in state.inventory.values():
        add_item_into_sea(state, item, item.qty)

    state.ship_size = 10
    state.inventory = Inventory(
        {
            'Fishing License': all_items.get('Fishing License'),
            'Insurance Cheque': {
                'use_type': ItemType.NO_TYPE, 'buy': 0, 'sell': items_lost * 10,
                'text': '$10.00 per item lost - just enough money for a fresh start',
                'flavour':
                    'It\'s not much, but it\'s better than nothing.'
            }
        }
    )


def test_for_sinking(state):
    """Test if the ship will sink."""

    excess_items = is_overloaded(state.inventory, state.ship_size)
    sink_power = excess_items - state.ship_size
    sink_chance = (sink_power / state.ship_size) * 10

    if random.randrange(0, 100) <= sink_chance:
        log_text(f'Your boat failed to stay afloat with all its cargo.')
        sink_ship(state)
        return True

    return False


def warn_of_sinking(state):
    """Warn of the ship's chance to sink."""

    excess_items = is_overloaded(state.inventory, state.ship_size)
    sink_power = excess_items - state.ship_size
    sink_chance = (sink_power / state.ship_size) * 10
    if excess_items and sink_power > 0:
        log_text(f'\nWarning: your ship is extremely overloaded!')
        log_text(f'Your ship has a {percentage(sink_chance)} chance of sinking from its weight!\n')
