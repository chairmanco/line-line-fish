import random

from functions.items.item_utils import decrement_item_by_name, add_item_by_name
from functions.text.logging import log_text
from objects.data.items.live_catch import LiveCatch


def use_breeding_kits(state):
    """Attempt to use a breeding kits."""

    if state.inventory.get('Marine Creature Breeding Kit'):
        while attempt_breeding(state):
            pass


def attempt_breeding(state):
    """Attempt to use a breeding kit on all items in the boat."""

    for item in state.inventory.values():
        if apply_breeding_kit(state, item):
            return True

    return False


def apply_breeding_kit(state, first_mate):
    """Attempt to consume a breeding kit and set two newborns free."""

    if not isinstance(first_mate, LiveCatch):
        return False

    if first_mate.qty > 1:
        decrement_item_by_name(state, first_mate.name, 2)
        decrement_item_by_name(state, 'Marine Creature Breeding Kit')
        log_text(f'You used a Marine Creature Breeding Kit!')
        log_text(f'Released for breeding: {first_mate.name[19:]}')
        log_text(f'Released for breeding: {first_mate.name[19:]}')
        breed_organism(state, first_mate.catch.parent_species())
        return True

    second_mate = state.inventory.find_item_containing(
        first_mate.catch.parent_species(), first_mate.name
    )

    if second_mate and isinstance(second_mate, LiveCatch):
        decrement_item_by_name(state, first_mate.name)
        decrement_item_by_name(state, second_mate.name)
        decrement_item_by_name(state, 'Marine Creature Breeding Kit')
        log_text(f'You used a Marine Creature Breeding Kit!')
        log_text(f'Released for breeding: {first_mate.name[19:]}')
        log_text(f'Released for breeding: {second_mate.name[19:]}')
        breed_organism(state, first_mate.catch.parent_species())
        return True

    return False


def breed_organism(state, species):
    """Generate over 1000 individuals in the current sea."""

    if 'Endangered Sea Turtle' in species:
        log_text(f'As a reward for your conservation efforts, you have been granted turtle fishing'
                 f' rights!\n'
                 f'These rights allow you to legally catch Sea Turtles for "research purposes".\n')
        add_item_by_name(state, 'Turtle Conservation Papers')
        state.feat_book.toggle_feat(9, state)
        species = 'No-Longer-Endangered Sea Turtle'

    qty = random.randrange(1000, 2000)
    state.persistent_sea.increase_frequency(
        species, qty
    )
    log_text(f'Introduced {species} x {qty}.\n')
