from functions.generation.get_catch import generate_catch_data
from functions.items.item_utils import decrement_item_by_name
from functions.logic.death import fisherman_death
from functions.text.formatting import kilograms, dollars
from functions.text.logging import log_text
from objects.data.fish_data.catch_data import CatchData
from objects.data.sea_objects.item_catch import ItemCatch


def process_statue(state, catch, sea):
    """Handle effects caused by statues and returns whether omens are warded against."""

    if state.statue_challenge:
        perform_lady_challenge(state, catch)

    if state.inventory.get('Lady of the Sea Statue'):
        if catch.contains('Omen'):
            deflect_omen(state, sea)
        return True

    return False


def perform_lady_challenge(state, catch):
    """Participate in the Lady's challenge."""

    lady_catch = state.statue_challenge['catch']
    state.statue_challenge['remaining_tries'] -= 1
    remaining = state.statue_challenge['remaining_tries']
    descriptor = (f'{lady_catch}'
                  f' ({kilograms(lady_catch.weight)}, {dollars(lady_catch.value)})')

    if catch.value >= lady_catch.value:
        log_text(f'The Lady is satisfied with your catch.\n')
        catch.value = 0
        state.statue_challenge = None
    elif remaining > 0:
        log_text(f'The Lady\'s challenge: {descriptor}\nYour offering: {dollars(catch.value)}')
        log_text(f'You have {remaining} {"tries" if remaining > 1 else "try"} left.\n')
    else:
        log_text(f'You have failed the Lady\'s challenge!'
                 f' ({dollars(catch.value)} < {dollars(lady_catch.value)})\n'
                 f'Her curse befalls you, bringing your death.\n')
        fisherman_death(state)
        log_text()
        catch.value = 0
        state.statue_challenge = None


def deflect_omen(state, sea):
    """Deflect an omen using the Lady of the Sea Statue."""

    lady_catch = generate_catch_data(state, sea)

    if (isinstance(lady_catch, ItemCatch)
            and lady_catch.get_item().contains('Love Letter to the Lady of the Sea')):
        log_text('The Lady of the Sea affords you her blessing and wards against the omen.')
        log_text(f'Instead of a fish, she finds a letter addressed to her.')
        log_text(f'Her statue begins to crumble, its pieces falling into the ocean.')
        log_text(f'You feel a rush of good fortune fill you.')
        state.luck += 24
        state.feat_book.toggle_feat(8, state)
        decrement_item_by_name(state, 'Lady of the Sea Statue')
    else:
        if isinstance(lady_catch, CatchData):
            descriptor = (f'({lady_catch} -'
                          f' {kilograms(lady_catch.weight)}, {dollars(lady_catch.value)})')
        else:
            descriptor = f'({lady_catch})'

        log_text('The Lady of the Sea affords you her blessing and wards against the omen.')
        log_text(f'A fish {descriptor} floats up dead,'
                 f' the mark of the Lady\'s tears on its skin.')
        log_text(f'You\'ll need to match her sacrifice within the next six catches or'
                 f' suffer her curse.\n')

        state.statue_challenge = {
            'catch': lady_catch,
            'remaining_tries': 6
        }
