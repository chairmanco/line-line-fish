import math

from functions.text.conditional_text import cond_print


def compute_bait(bait, money, cost):
    """Validate and correct bait value."""

    cond_print('Maximum bait: 200', bait > 200)
    bait = 200 * (bait > 200) + bait * (bait <= 200)

    cond_print('Minimum bait: 0', bait < 0)
    bait = 0 * (bait < 0) + bait * (bait >= 0)

    if cost > 0:
        cond_print(f'Affordable bait: {math.floor(money / cost)}',
                   bait * cost > money)
        bait = ((math.floor(money / cost)) * (bait * cost > money)
                + bait * (bait * cost <= money))

    return bait


def compute_chum(state, bait_cost):
    """Return discount from recycling chum if applicable."""

    if state.inventory.get('Legendary Bucket of Chum'):
        chum_discount = min(state.chum / 10, bait_cost)
        return chum_discount
    return 0
