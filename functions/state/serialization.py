import os
import pickle

from interface.config import config
from functions.text.logging import log_text


def dump_object(target, file_path=config.get('DEFAULT_PICKLE'), silent=False):
    """Serialize a Python object to a file."""

    if not os.path.exists(config.get('TEMP_PATH')):
        os.makedirs(config.get('TEMP_PATH'))

    try:
        with open(file_path, 'wb') as file:
            pickle.dump(target, file)
    except Exception as ex:
        if not silent:
            log_text(ex)


def load_object(file_path=config.get('DEFAULT_PICKLE'), silent=False):
    """Load a Python object from a file."""

    try:
        with open(file_path, 'rb') as file:
            return pickle.load(file)
    except Exception as ex:
        if not silent:
            log_text(ex)
