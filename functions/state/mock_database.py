from interface.config import config
from functions.state.database_manager import DatabaseManager
from functions.state.serialization import load_object
from objects.data.fisher import Fisher
from objects.data.location.location import Location
from objects.data.location.locations import Locations


class MockDatabase(DatabaseManager):
    """Pretends to manage connection to a database."""

    # TODO: This is a mock database for now
    TEMP_PATH = config.get('TEMP_PATH')

    def __init__(self):
        self.test_user = 'ChairmanCo'
        self.test_pass = '12345'
        DatabaseManager.__init__(self)

    def new_fisher(self, username, password):
        """Get a fisher from the database."""

        if username == self.test_user and password == self.test_pass:
            return False
        return True

    def get_fisher(self, username, password):
        """Get a fisher from the database."""

        if username == self.test_user and password == self.test_pass:
            state = Fisher(load_object(config.get('STATE_PICKLE'), True))
            if state.inventory is None:
                state = Fisher.definitive()
            state.save_to_file(config.get('STATE_PICKLE'))
            return state

        return None

    def reset_fisher(self, username, password):
        """Get a and reset a fisher from the database."""

        if username == self.test_user and password == self.test_pass:
            state = Fisher.definitive()
            if state.inventory is None:
                state = Fisher.definitive()
            state.save_to_file(config.get('STATE_PICKLE'))
            return state

        return None

    def save_fisher(self, username, password, state):
        """Save a fisher to the database."""

        if username == self.test_user and password == self.test_pass:
            state.save_to_file(config.get('STATE_PICKLE'))
            return True
        return False

    def try_login(self, username, password):
        """Validate user credentials."""

        if username == self.test_user and password == self.test_pass:
            return True
        return False

    def load_location(self, location_name):
        """Load a location."""

        location_path = location_name.replace(' ', '_')
        location_dict = load_object(f'{MockDatabase.TEMP_PATH}{location_path}.pickle', True)
        location = Location(location_name, location_dict)
        if location_dict is None:
            location = Locations.definitive().get_with_default_key(location_name, 'Default')
        location.save_to_file(f'{MockDatabase.TEMP_PATH}{location_path}.pickle')

        return location

    def save_location(self, location_name, location):
        """Save a location."""

        location_path = location_name.replace(' ', '_')
        location.save_to_file(f'{MockDatabase.TEMP_PATH}{location_path}.pickle')
        return True
