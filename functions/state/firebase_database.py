import pyrebase
from interface.config import config
from functions.state.database_manager import DatabaseManager
from objects.data.fisher import Fisher
from objects.data.location.location import Location
from objects.data.location.locations import Locations


class FirebaseDatabase(DatabaseManager):
    """Manages connection to the Line Line Fish Firebase database."""

    def __init__(self):
        self.db, self.db_user = FirebaseDatabase.get_database_and_db_user()

        self.username = None
        self.password = None
        DatabaseManager.__init__(self)

    @staticmethod
    def get_database_and_db_user():
        """Get a database object and a database user object."""

        app_config = {
            "apiKey": "AIzaSyCmCTx2B2c137cS9b52IcwzEFtO08lFldg",
            "authDomain": "line-line-fish.firebaseapp.com",
            "databaseURL": "https://line-line-fish.firebaseio.com",
            "storageBucket": "line-line-fish.appspot.com"
        }

        firebase = pyrebase.initialize_app(app_config)

        auth = firebase.auth()
        db_user = auth.sign_in_with_email_and_password(
            config.get('DATABASE_EMAIL'),
            config.get('DATABASE_PASSWORD')
        )

        db = firebase.database()

        return db, db_user

    def write_data(self, root_node, sub_node, data):
        """Write data to the database."""

        self.db.child(root_node).child(sub_node).set(data, self.db_user['idToken'])

    def read_data(self, root_node, sub_node):
        """Read data from the database."""

        data = self.db.child(root_node).child(sub_node).get(self.db_user['idToken'])

        return data.val()

    def new_fisher(self, username, password):
        """Get a fisher from the database."""

        if self.read_data('fishers', username):
            return False

        new_fisher = Fisher.definitive()
        self.write_data('fishers', username, {
            'username': username,
            'password': password,
            'fisher': new_fisher.to_dict()
        })
        return new_fisher

    def get_fisher(self, username, password):
        """Get a fisher from the database."""

        data = self.read_data('fishers', username)

        if data:
            if data['password'] == password:
                state = Fisher(data['fisher'])
                if state.inventory is None:
                    state = Fisher.definitive()
                    self.write_data('fishers', username, {
                        'username': username,
                        'password': password,
                        'fisher': state.to_dict()
                    })
                return state
        return None

    def reset_fisher(self, username, password):
        """Get a and reset a fisher from the database."""

        data = self.read_data('fishers', username)

        if data:
            if data['password'] == password:
                state = Fisher.definitive()
                if state.inventory is None:
                    state = Fisher.definitive()
                    self.write_data('fishers', username, {
                        'username': username,
                        'password': password,
                        'fisher': state.to_dict()
                    })
                return state
        return None

    def save_fisher(self, username, password, state):
        """Save a fisher to the database."""

        data = self.read_data('fishers', username)

        if data:
            if data['password'] == password:
                self.write_data('fishers', username, {
                    'username': username,
                    'password': password,
                    'fisher': state.to_dict()
                })
                return True
        return False

    def try_login(self, username, password):
        """Validate user credentials."""

        data = self.read_data('fishers', username)

        if data:
            if data['password'] == password:
                return True
        return False

    def load_location(self, location_name):
        """Load a location."""

        location_node = location_name.replace(' ', '_')
        data = self.read_data('locations', location_node)
        if data is None:
            location = Locations.definitive().get_with_default_key(location_name, 'Default')
        else:
            location = Location(location_name, data)

        self.write_data('locations', location_node, location.to_dict())
        return location

    def save_location(self, location_name, location):
        """Save a location."""

        location_node = location_name.replace(' ', '_')
        self.write_data('locations', location_node, location.to_dict())
        return True
