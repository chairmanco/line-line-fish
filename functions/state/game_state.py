from functions.state.firebase_database import FirebaseDatabase
from functions.text.logging import log_text
from interface.config import config
from interface.exit import exit_game
from objects.data.fisher import Fisher


def login(username, password):
    """Set user credentials in the state."""

    return game_state.login(username, password)


def new_state(username, password):
    """Create a new fisher."""

    state = game_state.new_state(username, password)

    if isinstance(state, Fisher):
        state.location_object = game_state.load_location(state.location)
    return state


def load_state(reset=False):
    """Load the fisher from the state and produce new values if needed."""

    state = game_state.load_state(reset)

    if isinstance(state, Fisher):
        state.location_object = game_state.load_location(state.location)

    return state


def save_state(state):
    """Save the fisher to the state."""

    if isinstance(state, Fisher):
        game_state.save_location(state.location, state.location_object)
        game_state.save_state(state)


class GameState:
    """Handles credentials and modifications to the game state."""

    def __init__(self, database_manager):
        self.database = database_manager
        self.username = config.get('DEFAULT_USERNAME')
        self.password = config.get('DEFAULT_PASSWORD')

    def login(self, username, password):
        """Set user credentials."""

        if self.database.try_login(username, password):
            self.username = username
            self.password = password
            return True
        else:
            log_text(f'Login failed for user {username}.')
            return False

    def new_state(self, username, password):
        """Create a new fisher."""

        new_fisher = self.database.new_fisher(username, password)
        if new_fisher:
            new_fisher.name = username
            return new_fisher
        else:
            log_text(f'Failed to create user {username}.')
            return None

    def load_state(self, reset=False):
        """Load the fisher and produce new values if needed."""

        if reset:
            state = self.database.reset_fisher(self.username, self.password)
        else:
            state = self.database.get_fisher(self.username, self.password)

        if state:
            if not state.name:
                state.name = self.username
            return state
        else:
            log_text(f'Loading failed for user {self.username}.')
            exit_game()

    def save_state(self, state):
        """Save the fisher."""

        if self.database.save_fisher(self.username, self.password, state):
            return True
        else:
            log_text(f'Saving failed for user {self.username}.')
            exit_game()

    def load_location(self, location_name):
        """Load a location and produce new values if needed."""

        location = self.database.load_location(location_name)

        if location:
            return location
        else:
            log_text(f'Loading failed for location {location_name}.')
            exit_game()

    def save_location(self, location_name, location):
        """Save a location."""

        if self.database.save_location(location_name, location):
            return True
        else:
            log_text(f'Saving failed for location {location_name}.')
            exit_game()


game_state = GameState(FirebaseDatabase())
