class DatabaseManager:
    """Manages database connections."""

    def __init__(self):
        pass

    def new_fisher(self, username, password):
        """Get a fisher from the database."""

        pass

    def get_fisher(self, username, password):
        """Get a fisher from the database."""

        pass

    def reset_fisher(self, username, password):
        """Get a and reset a fisher from the database."""

        pass

    def save_fisher(self, username, password, state):
        """Save a fisher to the database."""

        pass

    def try_login(self, username, password):
        """Validate user credentials."""

        pass

    def load_location(self, location_name):
        """Load a location."""

        pass

    def save_location(self, location_name, location):
        """Save a location."""

        pass
